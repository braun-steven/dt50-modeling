Classification report: 
             precision    recall  f1-score   support

 Above 120d       0.66      0.76      0.71       822
 Below 120d       0.94      0.91      0.92      3479

avg / total       0.89      0.88      0.88      4301

Accuracy score: 0.8793303882817949
Log loss: 0.6987533758230645
AUC Above 120d: 0.9104
AUC Below 120d: 0.9104

Confusion matrix, without normalization
[[ 621  201]
 [ 318 3161]]
Normalized confusion matrix
[[ 0.76  0.24]
 [ 0.09  0.91]]
Estimator: 
RandomForestClassifier(bootstrap=True, class_weight=None, criterion='gini',
            max_depth=None, max_features='auto', max_leaf_nodes=None,
            min_impurity_split=1e-07, min_samples_leaf=1,
            min_samples_split=2, min_weight_fraction_leaf=0.0,
            n_estimators=200, n_jobs=4, oob_score=False, random_state=42,
            verbose=0, warm_start=False) 
Options: 
cache = True 
clfs = ['rf'] 
cv = 10 
data_dir = data 
debug = False 
features = [<Feature.FPS: 0>, <Feature.RULES: 2>] 
fp = False 
fpsc = False 
fptype = maccs 
interval = None 
limit = True 
n_jobs = 4 
out = run/ 
regression = False 
sc = False 
test_size = 0.33 
verbose = 0 