# bachelor-thesis

## Todo
### General
- ~~EM-Imputation~~
- ~~Multiclass: Intervals (size?)~~
- ~~Cluster reactions~~
- ~~SVR: Different Kernels for different feature types~~
- ~~Multiple kernel learning~~
- ~~Gaussian process~~
- ~~Set-Kernel~~
- ~~Triggered rules as features~~
- ~~Gridsearch -> basinhoppin~~

### Denoising Autoencoder
- Test zero mask noise vs random noise


#### Intervals
Available through the following option with the intervals [0-50),[50-120),[120-inf)
```
--interval 0 50 120
```

#### Basinhopping
Tested but not yet effective since it cannot be parallelized as nice as GridSearch


