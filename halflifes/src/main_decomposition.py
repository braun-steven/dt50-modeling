import argparse

import numpy as np
import pandas as pd
from imblearn.over_sampling import SMOTE
from imblearn.pipeline import Pipeline
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import Imputer, StandardScaler
from collections import Counter
from Utils import data_utils, evaluator
from sklearn.decomposition import NMF

from tqdm import tqdm
from typing import Tuple
from subprocess import call
import subprocess
import os


def gen_matrices(X_scen: pd.DataFrame,
                 X_fp: pd.DataFrame,
                 y: pd.DataFrame,
                 opts: argparse.Namespace) \
        -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """
    Generates three matrices:
        - fp: matrix of unique compounds
        - scen: matrix of unique scenarios
        - DT50: matrix of all combinations of compounds and scenarios
    :param opts: commandline parameters
    :return: Tuple(scen,fp,DT50)
    """

    DT50, fp_array, scen_array = gen_dt50_matrix(X_fp, X_scen, y)

    U, V = exec_bmad(DT50, opts)

    return scen_array, fp_array, U, V, DT50


def gen_dt50_matrix(X_fp, X_scen, y):
    # Sets to check if rows in scen or fp already occured
    scen_set = set()
    fp_set = set()
    # Indices for counting and assignment
    scen_idx = -1
    fp_idx = -1
    scen_unique = []
    fp_unique = []
    # Replace np.nan by '?' since (np.nan == np.nan) evaluates to false
    X_scen = X_scen.fillna('?')
    X_fp = X_fp.fillna('?')
    m = {}
    for (idx, row_scen), (idx, row_fp) in zip(X_scen.iterrows(),
                                              X_fp.iterrows()):

        scen_tup = tuple(row_scen.values)
        if scen_tup not in scen_set:
            scen_set.add(scen_tup)
            scen_idx += 1
            scen_unique.append(row_scen.values)

        fp_tup = tuple(row_fp.values)
        if fp_tup not in fp_set:
            fp_set.add(fp_tup)
            fp_idx += 1
            fp_unique.append(row_fp.values)

        # Store the dt50 value for the current combination of compound and
        # scenario
        m[(fp_idx, scen_idx)] = y[idx]
    DT50 = np.ndarray(shape=(fp_idx + 1, scen_idx + 1), dtype=np.bool)
    for (x, y), dt50 in m.items():
        DT50[x, y] = dt50
    scen_array = np.array(scen_unique)
    fp_array = np.array(fp_unique)
    # Replace '?' with np.nan
    scen_array[scen_array == '?'] = np.nan

    scen_array = scen_array.astype(np.float64)
    fp_array = fp_array.astype(np.float64)
    return DT50, fp_array, scen_array


def parse_arg() -> argparse.Namespace:
    """Parse commandline arguments
    :return: argument dict
    """
    print('Parsing arguments')
    parser = argparse.ArgumentParser('Halflife prediction')
    parser.add_argument('-k', type=int, default=5,
                        help='Size of the decomposition components (m x k), '
                             '(k x n).')
    parser.add_argument('--tmpdir', type=str, default='tmp/',
                        help='Directory for temporary files.')


    parser.add_argument('--data-dir', '-d', default='data', type=str,
                        help='Data directory')
    parser.add_argument('--out', '-o', default='run', type=str,
                        help='Output directory')
    parser.add_argument('--cache', '-c', default=False, action='store_true',
                        help='Use the cache')
    parser.add_argument('--verbose', '-v', default=0, type=int,
                        help='Verbosity level')
    parser.add_argument('--n-jobs', '-n', type=int, default=4,
                        help='Number of parallel jobs')
    parser.add_argument('--cv', '-cv', type=int, default=10,
                        help='Number of cross validations')
    parser.add_argument('--clfs', type=str, nargs='+',
                        help='Classifier list (seperate by space)')
    parser.add_argument('--interval', type=float, nargs='+',
                        help='Interval: seperated by borders (e.g. "0 90 120") (seperated by space)')
    parser.add_argument('--features', '-f', type=str, nargs='+',
                        help='Features - can be a list of {"fp", "scen", '
                             '"rule"}. E.g. "fp scen" or "fp scen rule".')
    parser.add_argument('--test-size', type=float, default=0.33,
                        help='Size of the test-set in percent (currently only used in gridsearch)')
    parser.add_argument('--fptype', default='maccs', type=str,
                        help='Fingerprinter type.')
    parser.add_argument('--debug', default=False, action='store_true',
                        help='Enable debug mode')
    return parser.parse_args()


def exec_bmad(DT50, opts):
    data_utils.ensure_dir(opts.tmpdir)
    prefix = opts.tmpdir
    u_path = prefix + "U.csv"
    v_path = prefix + "V.csv"

    print('Executing bmad...')
    print('-' * 80)
    header = ','.join([str(x) for x in range(DT50.shape[1])])
    dt50_path = prefix + 'dt50.csv'
    np.savetxt(dt50_path, DT50, delimiter=',', header=header)
    k = str(opts.k)
    call([
        "java",
        "-cp",
        "bmad-exec-1.0-SNAPSHOT-jar-with-dependencies.jar",
        "Main",
        dt50_path,
        k,
        u_path,
        v_path
    ],stdout=open(os.devnull, 'w'), stderr=subprocess.STDOUT)
    print('Finished bmad...')
    print('-' * 80)

    U = np.loadtxt(u_path, dtype=int, skiprows=1, delimiter=',').astype(bool)
    V = np.loadtxt(v_path, dtype=int, skiprows=1, delimiter=',').astype(bool)

    return U, V


def main(opts: argparse.Namespace):
    # Get scenario data
    X_scen, y_scen = data_utils.load_scen_data_encoded(opts.data_dir,
                                                       cache=opts.cache,
                                                       opts=opts)

    # Get fingerprint data
    X_fp, y_fp = data_utils.load_fp_data(opts.data_dir,
                                         cache=opts.cache, opts=opts)

    y = y_scen < 120

    if opts.debug:
        X_scen = X_scen[0:200]
        X_fp = X_fp[0:200]
        y = y[0:200]

    X_train, X_test, y_train, y_test = train_test_split(
        list(zip(X_scen, X_fp)),
        y, test_size=0.33, random_state=42)

    X_scen_train = pd.DataFrame([x[0] for x in X_train])
    X_fp_train = pd.DataFrame([x[1] for x in X_train])

    X_scen_test = pd.DataFrame([x[0] for x in X_test])
    X_fp_test = pd.DataFrame([x[1] for x in X_test])

    y_train = y_train
    y_test = y_test

    # Get the input matrices
    scens_train, comps_train, U_train, V_train, DT50_train = gen_matrices(
        X_scen=X_scen_train,
        X_fp=X_fp_train,
        y=y_train,
        opts=opts
    )

    # Get the input matrices
    scens_test, comps_test, U_test, V_test, DT50_test = gen_matrices(
        X_scen=X_scen_test,
        X_fp=X_fp_test,
        y=y_test,
        opts=opts
    )

    print('Train shapes: ')
    print_shapes(scens_train, comps_train, U_train, V_train, DT50_train)
    print('Test shapes: ')
    print_shapes(scens_test, comps_test, U_test, V_test, DT50_test)

    '''
    Goal:   Scenarios predict U
            Compounds predict V
            Reconstruct DT50 = U x V
    
    Train two classifier on (scens, U) and (comps, V)
    '''

    # Init base classifier
    rf_scen = RandomForestClassifier(n_estimators=100)
    rf_comp = RandomForestClassifier(n_estimators=100)

    # Create pipes
    pipe_scen = get_pipe(rf_scen, opts)
    pipe_comp = get_pipe(rf_comp, opts)

    # Fit
    pipe_scen.fit(scens_train, V_train.T)
    pipe_comp.fit(comps_train, U_train)

    # Pred probas
    V_pred_proba = pipe_scen.predict_proba(scens_test)
    U_pred_proba = pipe_comp.predict_proba(comps_test)
    V_pred = pipe_scen.predict(scens_test)
    U_pred = pipe_comp.predict(comps_test)
    # Preds
    # V_pred_argmax = np.argmax(V_pred_proba, axis=1)
    # U_pred_argmax = np.argmax(U_pred_proba, axis=1)
    # V_pred = np.array([rf_scen.classes_[0][i] for i in V_pred_argmax])
    # U_pred = np.array([rf_comp.classes_[0][i] for i in U_pred_argmax])


    print('U_pred   shape: {}'.format(U_pred.shape))
    print('V_pred   shape: {}'.format(V_pred.shape))
    DT50_pred = U_pred.dot(V_pred.T).astype(bool)

    dt50_acc = accuracy_score(y_true=DT50_test.reshape(-1),
                              y_pred=DT50_pred.reshape(-1))
    U_acc = accuracy_score(y_true=U_test.reshape(-1),
                           y_pred=U_pred.reshape(-1))
    V_acc = accuracy_score(y_true=V_test.reshape(-1),
                           y_pred=V_pred.reshape(-1))

    def baseline(count):
        sum = count[True] + count[False]
        if count[True] > count[False]:
            return float(count[True]) / sum

        else:
            return float(count[False]) / sum
    dt50_count = Counter(DT50_test.reshape(-1))
    dt50_baseline = baseline(dt50_count)
    print('DT50 Accuracy:            {}'.format(round(dt50_acc, 3)))
    print('DT50 Baseline:            {}'.format(round(dt50_baseline, 3)))
    print('DT50_test distribution:   {}'.format(dt50_count))

    u_count = Counter(U_test.reshape(-1))
    u_baseline = baseline(u_count)
    print('U Accuracy:               {}'.format(round(U_acc, 3)))
    print('U Baseline:               {}'.format(round(u_baseline, 3)))
    print('U_test distribution:      {}'.format(u_count))

    v_count = Counter(V_test.reshape(-1))
    v_baseline = baseline(v_count)
    print('V Accuracy:               {}'.format(round(V_acc, 3)))
    print('V Baseline:               {}'.format(round(v_baseline, 3)))
    print('V_test distribution:      {}'.format(v_count))


def print_shapes(scens, comps, U, V, DT50):
    print('Scenario  shape: {}'.format(scens.shape))
    print('Compounds shape: {}'.format(comps.shape))
    print('U         shape: {}'.format(U.shape))
    print('V         shape: {}'.format(V.shape))
    print('DT50      shape: {}'.format(DT50.shape))


def get_pipe(clf, opts):
    return Pipeline(
        steps=[
            ('imputer', Imputer(strategy='mean')),
            ('scaler', StandardScaler()),
            # Impute missing values
            # ('sm', SMOTE(random_state=42, n_jobs=opts.n_jobs)),
            # Upsampling
            ('clf', clf)])  # Classify


if __name__ == '__main__':
    opts = parse_arg()
    main(opts)
    exit(0)
