from argparse import Namespace
from random import shuffle
from time import strftime, gmtime
import os
from keras import backend as K
import sys

from keras.engine import Model
from keras.optimizers import Adam, Nadam
from sklearn import model_selection
from sklearn.model_selection import GroupShuffleSplit

from Utils.data_utils import make_groups

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import keras
from keras.callbacks import TensorBoard, EarlyStopping
from sklearn.base import BaseEstimator, RegressorMixin, TransformerMixin
import numpy as np

from Utils.network_utils import construct_network, trainable, add_noise, \
    LayerSet, r2


class HLNetwork(BaseEstimator, RegressorMixin, TransformerMixin):
    def __init__(self, opts: Namespace, fp_dim, comp_desc_dim, hist_fp_dae=None,
                 hl_hists=None, hist_scen_dae=None):
        self.opts = opts
        self.fp_dim = fp_dim
        self.comp_desc_dim = comp_desc_dim
        self.hist_fp_dae = hist_fp_dae
        self.hl_hists = hl_hists
        self.hist_scen_dae = hist_scen_dae

    def fit(self,
            X_train: np.ndarray,
            y_train: np.ndarray):
        """
        Fit the model on the training data
        :param X_train: Training input
        :param y_train: Training target
        :return: 
        """

        X_fp = X_train[:, :self.fp_dim]
        X_comp_desc = X_train[:, self.fp_dim:self.fp_dim + self.comp_desc_dim]
        X_scen = X_train[:, self.fp_dim + self.comp_desc_dim:]

        if self.opts.debug:
            X_fp = X_fp[0:50]
            X_scen = X_scen[0:50]
            X_comp_desc = X_comp_desc[0:50]
            X_train = X_train[0:50]
            y_train = y_train[0:50]
            self.opts.num_switches = 2
            self.opts.epochs_dae = 5
            self.opts.epochs_hl = 6

        # Get the network parts
        autoencoder_fp, \
        autoencoder_scen, \
        encoder_fp, \
        encoder_scen, \
        hl_layer_set, hl_model = \
            construct_network(self.opts,
                              input_dim_fp=X_fp.shape[1],
                              input_dim_comp_desc=X_comp_desc.shape[1],
                              input_dim_scen=X_scen.shape[1],
                              kernel_reg=self.opts.kernel_reg,
                              act_reg=self.opts.act_reg)

        self.dae_fp = autoencoder_fp
        self.dae_scen = autoencoder_scen
        self.encoder_fp = encoder_fp
        self.encoder_scen = encoder_scen

        # Make only autoencoder_fp weights trainable
        trainable(autoencoder_fp, True, self.opts)
        trainable(autoencoder_scen, False, self.opts)
        trainable(hl_layer_set, False, self.opts)

        # 1. Train autoencoder for fingerprints
        self.start_fp_autoencoder(X_fp, autoencoder_fp)

        # Make only autoencoder_scen weights trainable
        trainable(autoencoder_scen, True, self.opts)
        trainable(autoencoder_fp, False, self.opts)

        # 2. Train autoencoder for scenarios
        self.start_scen_autoencoder(X_scen, autoencoder_scen)

        # 3. Train autoencoder alternating for halflife prediction
        trainable(autoencoder_scen, True, self.opts)
        trainable(hl_layer_set, True, self.opts)
        trainable(autoencoder_fp, True, self.opts)
        trainable(hl_model, True, self.opts)

        self.start_hl_training(X_fp, X_comp_desc, X_scen, X_train, hl_model,
                               y_train)

        return self

    def start_hl_training(self, X_fp, X_comp_desc, X_scen, X_train, hl_model,
                          y):
        # Generate validation data
        rs = GroupShuffleSplit(n_splits=1, test_size=self.opts.validation_split,
                               random_state=42)
        groups = make_groups(X_fp, X_scen)
        [(train, test)] = rs.split(X_train, y, groups=groups)
        X_scen_train, X_scen_test = X_scen[train], X_scen[test]
        X_fp_train, X_fp_test = X_fp[train], X_fp[test]
        y_train, y_test = y[train], y[test]
        num_switches = self.opts.num_switches
        epochs_per_switch = int(
            np.ceil(self.opts.epochs_hl / float(num_switches)))
        self.hl_hists = []
        adam = Adam(lr=self.opts.lr)


        skip_switches = self.opts.fp_encoding_dim == 0 or \
                        self.opts.scen_encoding_dim == 0

        if self.opts.limit:
            loss = 'binary_crossentropy'
            metrics = ['accuracy']
        elif self.opts.regression:
            loss = 'mse'
            metrics = ['mse', 'mae', r2]
        else:
            raise Exception('Either choose classification or regression')

        hl_model.compile(optimizer=adam, loss=loss, metrics=metrics)

        if skip_switches:
            num_switches = 1
        for i in range(num_switches):
            if not skip_switches:
                # Switch trainables
                if i % 2 == 0:
                    self.encoder_scen.trainable = False
                    self.encoder_fp.trainable = True
                else:
                    self.encoder_scen.trainable = True
                    self.encoder_fp.trainable = False

                # recompile models
                for model in [self.encoder_fp, self.encoder_scen, hl_model]:
                    model.compile(optimizer=adam, loss=loss, metrics=metrics)

            ep = (i + 1) * epochs_per_switch


            if skip_switches:
                ep = self.opts.epochs_hl

            # Fit model
            hl_hist = hl_model.fit(
                x=[X_fp_train, X_scen_train],
                y=y_train,
                epochs=ep,
                batch_size=self.opts.batch_size,
                shuffle=True,
                validation_data=(
                    [X_fp_test, X_scen_test],
                    y_test),
                verbose=self.opts.verbose,
                callbacks=[
                    EarlyStopping(monitor='val_loss',
                                  patience=self.opts.patience)
                ],
                initial_epoch=i * epochs_per_switch
            )

            self.hl_hists.append(hl_hist)
        self.hl_model = hl_model

    def start_scen_autoencoder(self, X_scen, autoencoder_scen):
        if self.opts.scen_encoding_dim == 0:
            print('Skipping autoencoder SCEN')
            return

        rs = GroupShuffleSplit(n_splits=1,
                               test_size=self.opts.validation_split,
                               random_state=42)
        X_scen = np.unique(X_scen, axis=0)
        # Generate validation data
        groups = [hash(x.tostring()) for x in X_scen]
        [(train, test)] = rs.split(X_scen, groups=groups)
        X_scen_train, X_scen_test = X_scen[train], X_scen[test]
        X_scen_train_noisy = add_noise(X_scen_train, self.opts.noise_factor)
        X_scen_test_noisy = add_noise(X_scen_test, self.opts.noise_factor)
        self.hist_scen_dae = autoencoder_scen.fit(
            x=X_scen_train_noisy,
            y=X_scen_train,
            epochs=self.opts.epochs_dae,
            batch_size=self.opts.batch_size,
            shuffle=True,
            validation_data=(X_scen_test_noisy, X_scen_test),
            verbose=self.opts.verbose,
            callbacks=[
                # TensorBoard(log_dir=logdir + '_scen',
                #             histogram_freq=0,
                #             write_graph=False),
                EarlyStopping(monitor='val_loss',
                              patience=self.opts.patience)
            ]
        )

    def start_fp_autoencoder(self, X_fp, autoencoder_fp):
        if self.opts.fp_encoding_dim == 0:
            print('Skipping autoencoder FP')
            return

        rs = GroupShuffleSplit(n_splits=1, test_size=self.opts.validation_split,
                               random_state=42)
        # Generate validation data
        X_fp = np.unique(X_fp, axis=0)
        groups = [hash(x.tostring()) for x in X_fp]
        [(train, test)] = rs.split(X_fp, groups=groups)
        X_fp_train, X_fp_test = X_fp[train], X_fp[test]
        X_fp_train_noisy = add_noise(X_fp_train, self.opts.noise_factor)
        X_fp_test_noisy = add_noise(X_fp_test, self.opts.noise_factor)
        self.hist_fp_dae = autoencoder_fp.fit(X_fp_train_noisy,
                                              X_fp_train,
                                              epochs=self.opts.epochs_dae,
                                              batch_size=self.opts.batch_size,
                                              shuffle=True,
                                              validation_data=(
                                                  X_fp_test_noisy, X_fp_test),
                                              verbose=self.opts.verbose,
                                              callbacks=[
                                                  # TensorBoard(log_dir=logdir + '_fp',
                                                  #             histogram_freq=0,
                                                  #             write_graph=False),
                                                  EarlyStopping(
                                                      monitor='val_loss',
                                                      patience=self.opts.patience)
                                              ])

    def predict(self,
                X_test: np.ndarray) -> np.ndarray:
        """
        Predict the y_test value
        :param X_test: Test data
        :return: Prediction of the test data
        """
        X_fp = X_test[:, :self.fp_dim]
        X_comp_desc = X_test[:, self.fp_dim:self.fp_dim + self.comp_desc_dim]
        X_scen = X_test[:, self.fp_dim + self.comp_desc_dim:]

        # return self.hl_model.predict([X_fp, X_comp_desc, X_scen])
        return self.hl_model.predict([X_fp, X_scen])

    def transform(self, X):
        """
        Transform the input data consisting of a compound part and scenario 
        part into its DAE encoded representation
        :param X: input
        :return: encoded input
        """
        X_fp = X[:, :self.fp_dim]
        X_comp_desc = X[:, self.fp_dim:self.fp_dim + self.comp_desc_dim]
        X_scen = X[:, self.fp_dim + self.comp_desc_dim:]

        input = X_fp
        model = Model(inputs=self.dae_fp.input,
                      outputs=self.hl_model.get_layer('conc').output)
        X_fp_enc = model.predict(input)

        return np.hstack((X_fp_enc, X_comp_desc, X_scen))

    def fit_transform(self, X, y=None, **fit_params):
        self.fit(X, y)
        return self.transform(X)

    def summary(self) -> str:
        return self.hl_model.summary()

    def histories(self):
        d = {}
        d['fp'] = self.hist_fp_dae
        d['scen'] = self.hist_scen_dae

        h = self.hl_hists[0]
        for i in range(1, len(self.hl_hists)):
            h.epoch += self.hl_hists[i].epoch
            for key, val in h.history.items():
                h.history[key] = val + self.hl_hists[i].history[key]

        d['hl'] = h
        return d

    def set_params(self, **params):
        for parameter, value in params.items():
            self.__setattr__(parameter, value)
        return self

    def get_params(self, deep=True):
        params = {'opts': self.opts,
                  'fp_dim': self.fp_dim,
                  'hist_fp_dae': self.hist_fp_dae,
                  'hl_hists': self.hl_hists,
                  'hist_scen_dae': self.hist_scen_dae
                  }
        sup_params = super().get_params()
        params.update(sup_params)
        return params
