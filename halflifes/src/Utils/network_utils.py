import os
import sys

import keras
from keras.optimizers import Adam
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import r2_score
from sklearn.preprocessing import PolynomialFeatures
from tqdm import tqdm

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from argparse import Namespace
from typing import Tuple, List, Union
import matplotlib

matplotlib.use("Agg")
from keras.layers import Input, Dense, concatenate, BatchNormalization, Dropout, \
    LeakyReLU, K
from keras.models import Model
import numpy as np


class LayerSet():
    def __init__(self, layers: List[Dense]):
        self._layers = layers
        self._trainable = True

    @property
    def trainable(self):
        return self._trainable

    @trainable.setter
    def trainable(self, value):
        assert type(value) == bool
        for l in self._layers:
            l.trainable = value

    def mean_weights_named(self):
        return [(l.name, np.mean([w.mean() for w in l.get_weights()])) for l in
                self._layers]

    def mean_weights(self):
        means = []
        for l in self._layers:
            if type(l) == Dense:
                weights = l.get_weights()
                means += [w.mean() for w in weights]

        return np.mean(means)


def construct_network(opts: Namespace,
                      input_dim_fp: int,
                      input_dim_comp_desc: int,
                      input_dim_scen: int,
                      kernel_reg: float = 0.01,
                      act_reg: float = 0.00) \
        -> Tuple[Model, Model, Model, Model, LayerSet, Model]:
    """
    Constructs the model architecture
    :param opts: Options
    :param input_dim_fp: Input dimension for fp encoder
    :param input_dim_scen: Input dimension for scen encoder
    :return: autoencoder for fp and scen, model for hl and hl layers
    """

    input_fp = Input(shape=(input_dim_fp,))
    input_scen = Input(shape=(input_dim_scen,))

    encoded_fp, autoencoder_fp = construct_autoencoder(opts.fp_encoding_dim,
                                                       input_fp, input_dim_fp,
                                                       opts.compression_rate,
                                                       'fp',
                                                       kernel_reg=kernel_reg,
                                                       act_reg=act_reg,
                                                       output_activation='sigmoid')

    encoded_scen, autoencoder_scen = construct_autoencoder(
        opts.scen_encoding_dim,
        input_scen,
        input_dim_scen,
        opts.compression_rate,
        'scen',
        kernel_reg=kernel_reg,
        act_reg=act_reg,
        output_activation='linear')

    layers = []

    ####################
    # Combine the encoded features of
    # scenarios and fingerprints and put it
    # into another network
    ####################

    # Build compression layers with the given compression rate
    layer_dims = []
    d = opts.fp_encoding_dim + opts.scen_encoding_dim
    while d * opts.compression_rate > 1:
        d = int(d * opts.compression_rate)
        layer_dims.append(d)

    # Concatenate the two autoencoder layers
    if opts.fp_encoding_dim == 0:
        if opts.scen_encoding_dim == 0:
            x = concatenate([input_fp, input_scen], axis=1, name='conc')
        else:
            x = concatenate([input_fp, encoded_scen], axis=1, name='conc')
    else:
        if opts.scen_encoding_dim == 0:
            x = concatenate([encoded_scen, input_scen], axis=1, name='conc')
        else:
            x = concatenate([encoded_fp, encoded_scen], axis=1, name='conc')

    for d in layer_dims:
        x = Dense(units=d,
                  kernel_regularizer=keras.regularizers.l2(kernel_reg),
                  activity_regularizer=keras.regularizers.l2(act_reg),
                  name='hl_dense_{}'.format(d))(x)
        x = LeakyReLU(alpha=0.1)(x)
        x = BatchNormalization(axis=-1)(x)
        layers.append(x)

    act = get_activation_function(opts)
    x = Dense(units=1,
              activation=act,
              name='hl_pred',
              kernel_regularizer=keras.regularizers.l2(kernel_reg),
              activity_regularizer=keras.regularizers.l2(act_reg)
              )(x)
    layers.append(x)

    # Collect the layers which are used for the halflife prediction
    hl_layer_set = LayerSet(layers)

    hl_model = Model(inputs=[input_fp, input_scen], outputs=x)

    if opts.limit:
        loss = 'binary_crossentropy'
        metrics = ['accuracy']
    elif opts.regression:
        loss = 'mse'
        metrics = ['mse', 'mae', r2]
    else:
        raise Exception('Either choose classification or regression')

    # Set the training configuration
    compile(hl_model, loss=loss, metrics=metrics, optimizer=Adam(lr=opts.lr))
    compile(autoencoder_scen, optimizer=Adam(lr=opts.lr))
    compile(autoencoder_fp, optimizer=Adam(lr=opts.lr))

    encoder_fp = Model(inputs=input_fp, outputs=encoded_fp)
    encoder_scen = Model(inputs=input_scen, outputs=encoded_scen)

    return autoencoder_fp, autoencoder_scen, \
           encoder_fp, encoder_scen, \
           hl_layer_set, hl_model,


# def r2(y_true, y_pred):
#     mae = y_true - y_pred
#     nom = K.sum(K.pow(mae, 2))
#     var = y_true - K.mean(y_true)
#     den = K.sum(K.pow(var, 2))
#     lhs = nom / den
#     score = 1 - lhs
#     return score

def r2(y_true, y_pred):
    mae = y_true - y_pred
    nom = K.sum(K.pow(mae, 2))
    var = y_true - K.mean(y_true)
    den = K.sum(K.pow(var, 2))
    lhs = nom / den
    score = lhs
    return score

def get_activation_function(opts):
    # Set correct activation for the last layer
    if opts.limit:
        act = 'sigmoid'
    elif opts.regression:
        act = 'linear'
    else:
        raise Exception('Either choose classification or regression')
    return act


def compile(model: Model, optimizer=None, loss: str = 'mse',
            metrics=['mse', 'mae', r2], opts: Namespace = None):
    if optimizer is None:
        optimizer = Adam(lr=opts.lr)
    model.compile(optimizer=optimizer, loss=loss, metrics=metrics)


def trainable(layers: Union[Model, LayerSet], value: bool, opts: Namespace,
              comp: bool=True, optimizer=None):
    """
    Disables layers
    :param layers: Can be either a model or a layerset
    :return: None
    """
    if optimizer is None:
        optimizer = Adam(lr=opts.lr)
    layers.trainable = value
    if type(layers) == Model and comp:
        compile(model=layers, opts=opts,optimizer=optimizer)


def construct_autoencoder(encoding_dim, input_layer, input_dim,
                          compression_rate, name_prefix,
                          kernel_reg: float = 0.01,
                          act_reg: float = 0.00,
                          dropout_rate: float = 0.0,
                          output_activation: str = 'linear'):
    layer_dims = []
    d = input_dim

    # Prevent while loop from getting stuck
    if encoding_dim == 0:
        encoding_dim = int(input_dim/2)
    while d * compression_rate > encoding_dim:
        d = int(d * compression_rate)
        layer_dims.append(d)

    # Encode
    x = input_layer
    for d in layer_dims:
        x = Dense(units=d,
                  name='{}_dae_dense_{}'.format(name_prefix, d),
                  kernel_regularizer=keras.regularizers.l2(kernel_reg),
                  activity_regularizer=keras.regularizers.l2(act_reg))(x)
        x = LeakyReLU(alpha=0.1)(x)
        x = BatchNormalization(axis=-1)(x)

    encoded = Dense(units=encoding_dim,
                    kernel_regularizer=keras.regularizers.l2(kernel_reg),
                    activity_regularizer=keras.regularizers.l2(act_reg),
                    name='{}_encoded'.format(name_prefix))(x)
    encoded = LeakyReLU(alpha=0.1)(encoded)

    # Decode
    x = encoded
    for d in reversed(layer_dims):
        x = Dense(units=d,
                  kernel_regularizer=keras.regularizers.l2(kernel_reg),
                  activity_regularizer=keras.regularizers.l2(act_reg))(x)
        x = LeakyReLU(alpha=0.1)(x)
        x = BatchNormalization(axis=-1)(x)

    decoded = Dense(units=input_dim,
                    activation=output_activation,
                    name='{}_decoded'.format(name_prefix),
                    kernel_regularizer=keras.regularizers.l2(kernel_reg),
                    activity_regularizer=keras.regularizers.l2(act_reg)
                    )(encoded)

    autoencoder = Model(input_layer, decoded)

    return encoded, autoencoder


def add_noise(X: np.ndarray,
              noise_factor: float = 0.5) \
        -> np.ndarray:
    """Adds noise to the data.
    The data needs to have approx. zero mean and a standard deviation of one
    :param X: Input train data
    :param noise_factor: Scaling of the noise 
    :return: 
    """
    std = [x.std() for x in X.T]
    loc = [x.mean() for x in X.T]
    shape = X.shape
    noise = np.random.normal(loc=loc, scale=[s ** 2 for s in std], size=shape)
    X_noisy = X + noise_factor * noise

    return X_noisy
