import hashlib

import argparse
from pprint import pprint

import matplotlib
from typing import Dict, List

import pandas as pd
import pybel
from matplotlib.ticker import FormatStrFormatter
from scipy.interpolate import interp1d
from scipy.stats import skew
from sklearn.preprocessing import LabelEncoder

from Utils.misc import Feature
from Utils.args import parse_arg

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

from Utils.data_utils import load_dataframe, load_scen_data_encoded, \
    load_fp_data, ensure_dir

plt.style.use('ggplot')

import seaborn as sns
import numpy as np


def plot_corr_heatmap():
    X, y = load_scen_data_encoded('data', cache=True)

    # X, y = load_fp_data('data', cache=True)

    sns.set(context="paper", font="monospace")

    # Load the datset of correlations between cortical brain networks
    corrmat = X.corr()

    # Set up the matplotlib figure
    f, ax = plt.subplots(figsize=(12, 9))

    plt.xticks(rotation=45)
    plt.yticks(rotation=45)
    # Draw the heatmap using seaborn
    sns.heatmap(corrmat, vmax=.8, square=True)

    f.tight_layout()
    plt.savefig('test.png')


def plot_env_vars():
    X, y = load_scen_data_encoded('data', cache=True, ret_type='pd')

    f, ax = plt.subplots(figsize=(16, 9), ncols=2)
    # Set up the matplotlib figure
    prefix = 'environment-plots/'
    ensure_dir(prefix)
    dt50 = np.log1p(y)
    for column in X:
        col = X[column]
        plt.subplot(121)
        vals = col.values[~np.isnan(col.values)]

        if column == 'biomassEnd' or column == 'biomassStart':
            vals = vals[vals < 1000]
        elif column == 'spikeconcentration':
            vals = vals[vals < 25]
        elif column == 'waterstoragecapacity':
            vals = vals[vals < 200]

        plt.title('{} histogram'.format(column))
        print(column)
        # plt.hist(vals, alpha=0.75, bins=25)
        sns.distplot(vals, bins=30)
        plt.ylabel('Normalized occurrence')
        plt.xlabel('{}'.format(column))

        plt.subplot(122)
        plt.title('dt50 vs {}'.format(column))
        plt.scatter(col.values, dt50, alpha=0.75)
        plt.ylabel('log(dt50)')
        plt.xlabel('{}'.format(column))

        plt.savefig(prefix + column + '.png')
        plt.clf()


def plot_dt50_per_comp():
    X_fp, y = load_fp_data('data', cache=True, opts=parse_arg())
    comps = [hashlib.md5(x.tostring()).digest() for x in X_fp]
    comps = LabelEncoder().fit_transform(comps)
    d: Dict[str, List[float]] = dict()

    for a, b in zip(comps, y):
        if a in d.keys():
            d[a].append(b)
        else:
            d[a] = [b]

    sns.set(palette="deep", color_codes=True)
    sns.set_style('ticks')

    fig = plt.figure(figsize=(6, 4))
    fig.add_subplot(111)
    data = d.values()
    data = sorted(data, key=lambda x: np.mean(x))

    # Plot persistence line
    plt.plot([0, len(data)], [120, 120], '--', c='r', linewidth=1.0)

    # Plot mean line
    x_vals = np.arange(len(data), step=1)
    means = np.array([np.mean(d) for d in data])[x_vals]
    plt.plot(x_vals, means, '-',
             c='b',
             alpha=1,
             linewidth=1.5)

    # Plot vertical lines
    for i, x in enumerate(data):
        plt.plot((i, i), (np.min(x), np.max(x)), 'k-', c='g', alpha=0.8,
                 linewidth=0.75)

    # Plot mean line again to overdraw the vertical lines
    plt.plot(x_vals, means, '-', c='b',
             alpha=1,
             linewidth=1.5)

    # Plot persistence line again to overdraw the vertical lines
    plt.plot([0, len(data)], [120, 120], '--', c='r', linewidth=1.0)

    ax = fig.get_axes()[0]
    ax.tick_params(axis='x', colors='white')
    ax.set_yscale('log')
    plt.xlabel('Compounds')
    plt.ylabel('$DT_{50}$ in days')
    plt.legend(['Persistence criterion',
                'Mean $DT_{50}$/compound', '$DT_{50}$ range/compound'],
               loc='upper left',
               frameon=True,
               fancybox=False,
               facecolor='w')
    plt.tight_layout()
    fig.savefig('imgs/dt50-per-comp.png', dpi=300)


def plot_dt50_freq_hist():
    X_fp, y = load_fp_data('data', cache=True, opts=parse_arg())
    comps = [hashlib.md5(x.tostring()).digest() for x in X_fp]
    comps = LabelEncoder().fit_transform(comps)
    d: Dict[str, List[float]] = dict()

    # count dt50 values
    for a, b in zip(comps, y):
        if a in d.keys():
            d[a] += 1
        else:
            d[a] = 1

    freqs = d.values()

    f, ax = plt.subplots(figsize=(6, 4))

    # Set up the matplotlib figure
    prefix = 'imgs/'
    ensure_dir(prefix)

    vals = list(freqs)
    # plt.title('DT50 frequency distribution')
    sns.distplot(vals,
                 norm_hist=False, kde=False,
                 hist_kws={"alpha": 1.0}
                 )
    sns.set_style('ticks')

    plt.ylabel('Number of compounds')
    plt.xlabel('Number of $DT_{50}$ values per compound')
    plt.tight_layout()
    plt.savefig('imgs/dt50-comp-freqs.png', dpi=300)
    plt.clf()


def plot_compound(opts: argparse.Namespace):
    # df, _ = load_dataframe(opts.data_dir, opts.cache, opts)
    # smiles = df['compound-smiles']
    smiles = ['CC1=C(C(C(C(=O)[O-])Cl)OC1=O)Cl']
    mol = pybel.readstring('smi', smiles[0])
    plt.figure(figsize=(12, 9))
    mol.draw(True, 'mol.png')
    pass


def plot_omcontent_skewed(opts):
    X, y = load_scen_data_encoded('data', cache=True, ret_type='pd')

    f, ax = plt.subplots(figsize=(6, 4))

    # Set up the matplotlib figure
    prefix = 'skewness-comp/'
    ensure_dir(prefix)
    column = 'omcontent'
    col = X[column]
    vals = col.values[~np.isnan(col.values)]

    # plt.title('OC distribution')
    orig_skew = round(skew(vals), 2)
    sns.distplot(vals, bins=50, label='raw: {}'.format(orig_skew))

    log1p_vals = np.sign(vals) * np.log1p(np.abs(vals))

    log1p_skew = round(skew(log1p_vals), 2)
    sns.distplot(log1p_vals, bins=30, label='log$_{1p}$: ' + str(
        log1p_skew))
    plt.ylabel('Normalized occurrence')
    plt.xlabel('OC')
    plt.xlim((0, 6))
    plt.legend(loc='upper right', frameon=True, fancybox=False,
               facecolor='w', title='Skewness')

    # matplotlib.rc('font', **font)
    plt.savefig('imgs/omcontent-raw-vs-log1p.png', dpi=300)
    plt.clf()


def plot_skewness_thres_comp():
    a = np.loadtxt('/home/tak/dt50/halflifes/comps/2017-07-10_10-28'
                   '-09_skewness_thres/xy.csv', delimiter=',')
    x = a[:, 0]
    y = a[:, 1]

    xlabel = 'Skewness Threshold'
    ylabel = '$R^2$ Score'

    sns.set(palette="deep", color_codes=True)
    fig, ax1 = plt.subplots(figsize=(6, 4))

    ax1.plot(x, y, c='b', alpha=0.8)
    ax1.set_xlabel(xlabel)
    ax1.yaxis.set_major_formatter(FormatStrFormatter('%.3f'))
    ax1.set_ylabel(ylabel)

    plt.tight_layout()
    plt.savefig('imgs/skew_thres_comp.png', dpi=300)


def plot_autoencoder_losses(type):
    a = np.loadtxt('autoencoder/{}_y-y_val.csv'.format(type), delimiter=',')
    a = a[:100]
    y = a[:, 0]
    y_val = a[:, 1]

    import seaborn as sns
    # sns.set_style("whitegrid", {'axes.grid': False})

    cols = sns.color_palette('deep')
    blue = cols[0]
    green = cols[1]
    red = cols[2]

    plt.figure(figsize=(6, 4))
    # plt.title('{}: epochs vs {}'.format(net_part, metric))
    plt.plot(y, color=blue, alpha=0.7, label='train')
    plt.plot(y_val, color=green, alpha=0.7, label='validation')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.legend(loc='upper right', frameon=True, fancybox=False,
               facecolor='w')
    plt.savefig('autoencoder/{}_{}.png'.format(type + '_loss', None),
                dpi=300)
    plt.close()


def plot_autoencoder_set_comp():
    def get(t):
        c = np.loadtxt('autoencoder/{}_y-y_val.csv'.format(t),
                       delimiter=',')[:100]
        return c[:, 0], c[:, 1]

    comp_train, comp_val = get('compound')
    scen_train, scen_val = get('scenario')
    mnist_train, mnist_val = get('mnist')

    import seaborn as sns
    # sns.set_style("whitegrid", {'axes.grid': False})

    cols = sns.color_palette('deep')
    blue = cols[0]
    green = cols[1]
    red = cols[2]

    x = range(1, comp_train.shape[0] + 1)
    plt.figure(figsize=(6, 4))
    # plt.title('{}: epochs vs {}'.format(net_part, metric))
    plt.plot(x, comp_val, color=blue, alpha=0.85, label='Compound validation')
    plt.plot(x, comp_train, color=blue, alpha=0.3, label='Compounds training')
    plt.plot(x, scen_val, color=green, alpha=0.85, label='Scenarios validation')
    plt.plot(x, scen_train, color=green, alpha=0.3, label='Scenarios training')
    plt.plot(x, mnist_val, color=red, alpha=0.85, label='MNIST validation')
    plt.plot(x, mnist_train, color=red, alpha=0.3, label='MNIST training')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.legend(loc='upper right', frameon=True, fancybox=False,
               facecolor='w', fontsize=8)
    plt.savefig('autoencoder/loss_all.png', dpi=300)
    plt.close()


if __name__ == '__main__':
    # plot_env_vars()
    plot_dt50_per_comp()

    # plot_compound(parse_arg())
    # plot_omcontent_skewed(parse_arg())
    plot_dt50_freq_hist()
    # plot_skewness_thres_comp()
    # for f in ['compound', 'scenario', 'mnist']:
    #     plot_autoencoder_losses(f)

    # plot_autoencoder_set_comp()
