#!/bin/bash
fps=( maccs fp2 fp3 fp4  )
for fp in ${fps[@]}
do
	echo ">>>>>>>>>>>>> CURRENT FP: $fp"
	./env/bin/python src/main_regression.py  --regs rf_sqrt svr bagsvr --fptype $fp --n-jobs 4 --cv 10 --tag $fp
done
