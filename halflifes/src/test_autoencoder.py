import argparse

from keras.layers import Input, Dense, K
from keras.models import Model

# this is the size of our encoded representations
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import Imputer, StandardScaler, Normalizer
from sklearn.utils import shuffle

from Utils.network_utils import construct_autoencoder
from Utils.data_utils import load_fp_data, load_scen_data_encoded, ensure_dir
from Utils.args import parse_arg


def r2_modded(y_true, y_pred):
    mae = y_true - y_pred
    nom = K.sum(K.pow(mae, 2))
    var = y_true - K.mean(y_true)
    den = K.sum(K.pow(var, 2))
    lhs = nom / den
    score = lhs
    return score

def run(x_train, x_test, epochs, tag, orig_img_pixels, orig_img_height,
        orig_img_width,
        batch_size,
        enc_dim,
        enc_img_height, enc_img_width, cr,
        activation, opts):

    dir = 'autoencoder/'
    ensure_dir(dir)
    noise_factor = opts.noise_factor
    x_train_noisy = x_train + noise_factor * np.random.normal(loc=0.0,
                                                              scale=x_train.std(),
                                                              size=x_train.shape)
    x_test_noisy = x_test + noise_factor * np.random.normal(loc=0.0,
                                                            scale=x_test.std(),
                                                            size=x_test.shape)
    encoding_dim = enc_dim

    # this is our input placeholder
    input_dim = x_train.shape[1]
    input_img = Input(shape=(input_dim,))

    # this model maps an input to its encoded representation
    compression_rate = cr
    encoded, autoencoder = construct_autoencoder(encoding_dim, input_img,
                                                 input_dim,
                                                 compression_rate,
                                                 'mnist_ae', 0.0001, 0.000, 0,
                                                 activation)
    encoder = Model(input_img, encoded)

    # create a placeholder for an encoded (32-dimensional) input
    encoded_input = Input(shape=(encoding_dim,))
    # retrieve the last layer of the autoencoder model
    decoder_layer = autoencoder.layers[-1]
    # create the decoder model
    decoder = Model(encoded_input, decoder_layer(encoded_input))

    autoencoder.compile(optimizer='adam', loss=r2_modded)

    hist = autoencoder.fit(x_train_noisy, x_train,
                    epochs=epochs,
                    batch_size=batch_size,
                    shuffle=True,
                    validation_data=(x_test_noisy, x_test),
                    verbose=opts.verbose)

    # encode and decode some digits
    # note that we take them from the *test* set
    encoded_imgs = encoder.predict(x_test_noisy)
    decoded_imgs = decoder.predict(encoded_imgs)
    # use Matplotlib (don't ask)
    import matplotlib.pyplot as plt

    import seaborn as sns
    # sns.set_style("whitegrid", {'axes.grid': False})

    cols = sns.color_palette('deep')
    blue = cols[0]
    green = cols[1]
    red = cols[2]

    n = 4  # how many digits we will display
    plt.figure(figsize=(4, 4))
    fontsize = 7
    for i in range(n):
        # display original
        ax1 = plt.subplot(4, n, i + 1)
        ax1.grid(False)
        ax1.set_xlabel('({})'.format(i + 1), fontsize=fontsize)
        ax1.xaxis.set_label_position('top')
        plt.imshow(x_test[i][:orig_img_pixels].reshape(orig_img_height,
                                                             orig_img_width))
        plt.gray()
        # ax1.get_xaxis().set_visible(False)
        ax1.set_xticks([])
        if i == 0:
            plt.tick_params(
                axis='y',
                which='both',
                left='off',
                right='off',
                labelleft='off')
            plt.ylabel('Input', fontsize=fontsize)
        else:
            ax1.get_yaxis().set_visible(False)

        # display original
        ax2 = plt.subplot(4, n, i + 1 + n)
        ax2.grid(False)
        plt.imshow(x_test_noisy[i][:orig_img_pixels].reshape(orig_img_height,
                                                             orig_img_width))
        plt.gray()
        ax2.get_xaxis().set_visible(False)
        if i == 0:
            plt.tick_params(
                axis='y',
                which='both',
                left='off',
                right='off',
                labelleft='off')
            plt.ylabel('Input with Noise', fontsize=fontsize)
        else:
            ax2.get_yaxis().set_visible(False)

        # display reconstruction
        ax3 = plt.subplot(4, n, i + 1 + 2 * n)
        ax3.grid(False)
        plt.imshow(encoded_imgs[i].reshape(enc_img_height, enc_img_width))
        plt.gray()
        ax3.get_xaxis().set_visible(False)
        if i == 0:
            plt.tick_params(
                axis='y',
                which='both',
                left='off',
                right='off',
                labelleft='off')
            plt.ylabel('Encoded Input', fontsize=fontsize)
        else:
            ax3.get_yaxis().set_visible(False)

        # display reconstruction
        ax4 = plt.subplot(4, n, i + 1 + 3 * n)
        ax4.grid(False)
        plt.imshow(decoded_imgs[i][:orig_img_pixels].reshape(orig_img_height,
                                                             orig_img_width))
        plt.gray()
        ax4.get_xaxis().set_visible(False)
        if i == 0:
            plt.tick_params(
                axis='y',
                which='both',
                left='off',
                right='off',
                labelleft='off')
            plt.ylabel('Decoded Input', fontsize=fontsize)
        else:
            ax4.get_yaxis().set_visible(False)
    plt.savefig('{}{}_{}.png'.format(dir, tag, opts.tag), dpi=300)



    plt.figure(figsize=(6, 4))
    # plt.title('{}: epochs vs {}'.format(net_part, metric))
    metric = 'loss'
    y = hist.history[metric]
    y_val = hist.history['val_' + metric]
    plt.plot(y, color=blue, alpha=0.7, label='train')
    plt.plot(y_val, color=green, alpha=0.7, label='validation')
    plt.xlabel('epoch')
    plt.ylabel(metric.replace('_', ' '))
    plt.legend(loc='upper right', frameon=True, fancybox=False,
               facecolor='w')
    plt.savefig('{}{}_{}.png'.format(dir, tag + '_loss', opts.tag), dpi=300)
    plt.close()

    res = [(yp, yvalp) for yp, yvalp in zip(y, y_val)]
    np.savetxt(fname='{}{}_y-y_val.csv'.format(dir, tag), X=res, delimiter=',')

    mse = mean_squared_error(x_test, decoded_imgs)
    r2 = (r2_score(x_test, decoded_imgs) - 1) * (-1)
    print('MSE {}: {}'.format(tag, mse))
    print('Modded R2 {}: {}'.format(tag, r2))


if __name__ == '__main__':
    opts = parse_arg()

    from keras.datasets import mnist
    import numpy as np

    print('Running on compounds')
    # Compounds
    X, _ = load_fp_data('data', cache=opts.cache, opts=opts)
    X = np.unique(X, axis=0)
    epochs = 200
    if opts.debug:
        X = X[:20]
        epochs = 1
    x_train, x_test = train_test_split(X, random_state=42, test_size=0.2)
    run(x_train, x_test, epochs, 'compound', 160, 16, 10, 32, 32, 8, 4, 0.5,
        'sigmoid', opts)


    print('Running on Scenarios')
    # Scenarios standard scaled
    X, _ = load_scen_data_encoded('data', cache=opts.cache, opts=opts)
    X = np.unique(X, axis=0)

    epochs = 200
    if opts.debug:
        X = X[:20]
        epochs = 1
    X = Imputer().fit_transform(X)
    X = StandardScaler().fit_transform(X)
    x_train, x_test = train_test_split(X, random_state=42, test_size=0.2)

    run(x_train, x_test, epochs, 'scenario', 30, 6, 5, 32, 4, 2, 2, 0.5,
        'linear', opts)


    print('Running on MNIST')
    # Mnist
    (x_train, _), (x_test, _) = mnist.load_data()
    x_train = x_train.astype('float32') / 255.
    x_test = x_test.astype('float32') / 255.
    x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
    x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))
    epochs = 200
    if opts.debug:
        x_train = x_train[:20]
        x_test = x_test[:20]
        epochs = 1
    run(x_train, x_test, epochs, 'mnist', x_train.shape[1], 28, 28, 256, 32,
        8, 4, 0.5, 'sigmoid', opts)
