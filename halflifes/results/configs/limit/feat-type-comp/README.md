## Feature type comparison
- FP: features are only fingerprints generated from `openbabel.pybel`
- SCEN: features are only scenario parameters
- RULE: Rules triggered by compounds

### Results
| feature type | accuracy | ROC-AUC |
| --- | --- | --- |
| fp | 0.878 | 0.903 |
| scen | 0.820 | 0.781 |
| rule | 0.849 | 0.886|
| fp + scen | 0.909 | 0.929 |
| fp + rule | 0.879 | 0.910 |
| scen + rule | 0.869 | 0.868 |
| fp + scen + rule | 0.908 | 0.932 |

For repetition, execute with list of features chosen from `{fp, sc, fpsc}`
```bash
python src/main_classification.py -d data --clfs rf --limit --fptype maccs -c --features FEAT1 FEAT2 ...
```
### Notes
Ran with `RandomForestClassifier`, `maccs` fingerprinter and 5-Fold-CV.