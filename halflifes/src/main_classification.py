#!../env/bin/python

import argparse
from argparse import Namespace
from enum import Enum
from pprint import pprint
from typing import Tuple, List

import matplotlib
import pandas as pd
import pybel
import sys
from scipy.optimize import basinhopping
from sklearn import clone
from sklearn.decomposition import PCA
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier
from sklearn.utils import shuffle

matplotlib.use('Agg')
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import SGDClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import cross_val_predict as cvp

import matplotlib.pyplot as plt

plt.style.use('ggplot')
from sklearn.preprocessing import Imputer
from sklearn import model_selection
from sklearn.svm import SVC
from imblearn.over_sampling import SMOTE
from imblearn.pipeline import Pipeline

from Utils import evaluator
from Utils.data_utils import load_scen_data_encoded, \
    load_fp_data, ensure_dir, load_reactions
from Utils.model_util import get_clf
from Utils.misc import Feature


def replace_intervall(y: np.ndarray,
                      interval: List[int]):
    """
    Replaces the dt50 value with the lower bound of its interval.
    E.g. 56 would be replaced by 50 which is the lower bound of [50,60)
    """
    for i in reversed(interval):
        if y >= i:
            return i

    print("UNDEFINED:" + str(y))


def build_interval_classifier(opts: Namespace,
                              interval: list = None,
                              target_names: list = None):
    """Build and evaluate a classifier on the 'interval' task"""

    X, y = get_data(opts)

    y = np.array([replace_intervall(yp, interval) for yp in y])

    # Define clfs to be evaluated
    clfs = [get_clf(tp, opts) for tp in opts.clfs]

    for clf in clfs:
        try:
            print(
                'Fitting: ' + clf.__class__.__name__ + ' on ' + str(
                    opts.features))
            pipe = Pipeline(
                steps=[
                    ('imputer', Imputer(strategy='mean')),
                    ('scaler', StandardScaler()),
                    # ('sm', SMOTE(random_state=42, n_jobs=opts.n_jobs)),
                    ('clf', clf)])
            run_clf(X, y, pipe, opts.features, opts=opts,
                    target_names=target_names)

            print('_' * 80)
            print()
            print()
        except Exception as e:
            print('''
                Could not fit classifier:
                {}
                Exception: 
                {}
                Skipping...
            '''.format(clf, e))
            print('-' * 80)


def get_data(opts: argparse.Namespace) \
        -> Tuple[np.ndarray, np.ndarray]:
    """
    Gets the data based on the feat type
    :param opts: argparse namespace
    :return: X, y
    """

    def concat(X, X_fp, y):
        if X is None:
            X = X_fp
            y = y
        else:
            X = np.hstack((X, X_fp))
            y = y
        return X, y

    X, y = None, None
    for feat in opts.features:
        if feat == Feature.SCENS:
            X_scen, y = load_scen_data_encoded(opts.data_dir, cache=opts.cache,
                                               opts=opts)
            X, y = concat(X, X_scen, y)

        elif feat == Feature.FPS:
            X_fp, y = load_fp_data(opts.data_dir, cache=opts.cache, opts=opts)
            X, y = concat(X, X_fp, y)
        elif feat == Feature.RULES:
            X_rule, y = load_reactions(opts=opts)
            X, y = concat(X, X_rule, y)
    return X, y


def build_limit_classifier(opts: Namespace, limit: int = 120,
                           target_names: list = None):
    """Build and evaluate classifier on the 'limit' task"""
    features = opts.features
    X, y = get_data(opts)
    y = [target_names[1] if yp < limit else target_names[0] for yp in y]

    # Use only some small test examples in debug mode
    if opts.debug:
        X = X[0:150]
        y = y[0:150]
        mask = pd.isnull(X)
        X = np.array(X, dtype=np.float64)
        X[mask] = np.nan

    # Define clfs to be evaluated
    clfs = [get_clf(tp, opts) for tp in opts.clfs]

    for clf in clfs:
        print('Fitting: ' + clf.__class__.__name__ + ' on ' + str(features))
        pipe = Pipeline(
            steps=[
                ('imputer', Imputer(strategy="mean")),
                ('scaler', StandardScaler()),
                # Impute missing values
                ('sm', SMOTE(random_state=42, n_jobs=opts.n_jobs)),
                # Upsampling
                ('clf', clf)])  # Classify

        run_clf(X, y, pipe, features, target_names, opts)

        print('_' * 80)
        print()
        print()

    return


def run_clf(X: np.ndarray,
            y: np.ndarray,
            pipe: Pipeline,
            features: List[Feature],
            target_names: List[str],
            opts: argparse.Namespace) -> None:
    """Evaluate a classifier on the given data"""

    clf = pipe.named_steps['clf']
    if isinstance(clf, GridSearchCV):

        ### Grid-Search ###

        # Split in test and train
        X_train, X_test, y_train, y_test = model_selection.train_test_split(
            X, y, test_size=opts.test_size, random_state=42)
        pipe.fit(X_train, y_train)
        print(clf.__class__.__name__ + ' BEST PARAMS: ')
        print(clf.best_params_)

        best_clf = clf.best_estimator_

        # If Gridsearch's baseestimator was SVC, reenable probability=True
        if type(clf.best_estimator_) == SVC:
            params = clf.best_params_
            params['probability'] = True
            best_clf = SVC(**params)
            pipe.set_params(clf=best_clf)
            pipe.fit(X_train, y_train)

        pipe.set_params(clf=best_clf)
        print('Best estimator:')
        print(best_clf)

        y_pred = pipe.predict(X_test)
        y_pred_proba = pipe.predict_proba(X_test)

        # Evaluate
        eval = evaluator.Evaluator(best_clf, features)
        eval.evaluate_clf(
            y_test=y_test,
            y_pred=y_pred,
            y_pred_proba=y_pred_proba,
            target_names=target_names,
            opts=opts
        )

    else:

        X_fp, _ = load_fp_data(opts.data_dir, opts.cache, opts)

        groups = True
        if groups:
            rs = model_selection.GroupKFold(n_splits=opts.cv)

            X, y, groups = shuffle(X, y, groups, random_state=42)
            y_pred_proba = cvp(estimator=pipe,
                         X=X,
                         y=y,
                         cv=rs,
                         n_jobs=opts.n_jobs,
                         groups=groups,
                               method='predict_proba')
            y_pred_argmax = np.argmax(y_pred_proba, axis=1)
            y_pred = np.array([target_names[i] for i in y_pred_argmax])
        else:

            y_pred_proba = cvp(pipe, X, y,
                               cv=StratifiedKFold(
                                   n_splits=opts.cv,
                                   shuffle=True,
                                   random_state=42),
                               n_jobs=opts.n_jobs,
                               method='predict_proba',
                               verbose=opts.verbose)
            y_pred_argmax = np.argmax(y_pred_proba, axis=1)
            y_pred = np.array([target_names[i] for i in y_pred_argmax])

        # Evaluate
        eval = evaluator.Evaluator(clf, features)
        eval.evaluate_clf(
            y_test=y,
            y_pred=y_pred,
            y_pred_proba=y_pred_proba,
            target_names=target_names,
            opts=opts
        )


def parse_arg() -> Namespace:
    """Parse commandline arguments
    :return: argument dict
    """
    print('Parsing arguments')
    parser = argparse.ArgumentParser('Halflife prediction')
    parser.add_argument('--data-dir', '-d', default='data', type=str,
                        help='Data directory')
    parser.add_argument('--out', '-o', default='run', type=str,
                        help='Output directory')
    parser.add_argument('--cache', '-c', default=False, action='store_true',
                        help='Use the cache')
    parser.add_argument('--verbose', '-v', default=0, type=int,
                        help='Verbosity level')
    parser.add_argument('--n-jobs', '-n', type=int, default=4,
                        help='Number of parallel jobs')
    parser.add_argument('--cv', '-cv', type=int, default=10,
                        help='Number of cross validations')
    parser.add_argument('--clfs', type=str, nargs='+',
                        help='Classifier list (seperate by space)')
    parser.add_argument('--interval', type=float, nargs='+',
                        help='Interval: seperated by borders (e.g. "0 90 120") (seperated by space)')
    parser.add_argument('--features', '-f', type=str, nargs='+',
                        help='Features - can be a list of {"fp", "scen", '
                             '"rule"}. E.g. "fp scen" or "fp scen rule".')
    parser.add_argument('--test-size', type=float, default=0.33,
                        help='Size of the test-set in percent (currently only used in gridsearch)')
    parser.add_argument('--limit', '-l', default=False, action='store_true',
                        help='Start limit classification (120d).')
    parser.add_argument('--regression', '-r', default=False,
                        action='store_true',
                        help='Start regression')
    parser.add_argument('--fptype', default='maccs', type=str,
                        help='Fingerprinter type.')
    parser.add_argument('--debug', default=False, action='store_true',
                        help='Enable debug mode')

    opts = parser.parse_args()

    if opts.out[len(opts.out) - 1] != '/':
        opts.out += '/'

    features = []
    for feat in opts.features:
        if feat == 'fp':
            features.append(Feature.FPS)
        if feat == 'scen':
            features.append(Feature.SCENS)
        if feat == 'rule':
            features.append(Feature.RULES)
    opts.features = features

    return opts


if __name__ == '__main__':

    # Setup multiprocessing for mogon
    import multiprocessing as mp

    mp.set_start_method('forkserver', True)

    # Get parser arguments
    opts = parse_arg()

    # Ensure output directory
    ensure_dir(opts.out)

    if opts.limit:
        target_names = ["Above 120d", "Below 120d"]
        pprint('Options:')
        pprint(vars(opts), indent=2)
        build_limit_classifier(opts,
                               target_names=target_names)

    if opts.interval:
        interval = opts.interval
        target_names = interval

        build_interval_classifier(opts,
                                  target_names=target_names,
                                  interval=interval)
