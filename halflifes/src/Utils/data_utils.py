import argparse
import hashlib
import os
from collections import Counter
from pprint import pprint
from time import time

from typing import Tuple, Union, List
import scipy.sparse
import sys

from fancyimpute import MICE, SoftImpute, NuclearNormMinimization, KNN, \
    MatrixFactorization, BiScaler
from scipy.sparse import csr_matrix
import numpy as np
import pandas as pd
import pybel
import ujson as json

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.model_selection._split import _BaseKFold
from sklearn.preprocessing import StandardScaler, Imputer

DF_CACHED = None
INFO_CACHED = None
errors = {'humidity': 0, 'omcontent': 0, 'soil': 0}


def load_dataframe(data_dir: str,
                   cache: bool = True,
                   opts: argparse.Namespace = None) \
        -> Tuple[pd.DataFrame, dict]:
    """
    Loads the dataframe
    :param data_dir: data directory 
    :param cache: cache flag
    :param opts: options
    :return: dataframe, info
    """

    # Use caches if load_dataframe has been already called
    global DF_CACHED, INFO_CACHED
    if DF_CACHED is not None and INFO_CACHED is not None:
        return DF_CACHED, INFO_CACHED

    if cache:
        df = pd.read_csv('{}/data.csv'.format(data_dir))
        info = json.load(open('{}/info.json'.format(data_dir), 'r'))
        DF_CACHED, INFO_CACHED = df, info
        return df, info

    t0 = time()
    df = pd.read_csv('{}/data_raw.csv'.format(data_dir))

    # Drop unnecessary columns
    del df['biomass']
    del df['compoundLink']
    del df['fullname']
    del df['infoID']
    del df['name']
    del df['unit']
    del df['value']
    del df['dt50-first-order']

    if opts.filter_scenarios:
        # Further drop columns as proposed by Katrin Fenner
        drop = ['bulkdensity', 'redox', 'soilclassificationsystem',
                'spikeconcentration']
        for d in drop:
            del df[d]

    # Fix features
    df, info = transform_soil_texture_2(df)
    df, info = fix_acidity(df, info)
    df, info = fix_humidity(df, info)
    df, info = fix_soil_organic_content(df, info)

    df = df.replace('?', np.nan)

    # Add new features
    df, info = add_exp_moisture_content(df, info)
    df, info = add_biomass_diff(df, info)

    # Try to fix dtype
    for col in df:
        try:
            df[col] = pd.to_numeric(df[col])
        except Exception:
            pass

    # Filter dataframe based on several conditions
    before = df.shape[0]
    if opts.filter_df:
        print('Size before filtering: ', before)
        df = filter_df(df)
        after = df.shape[0]
        print('Size are filtering: ', after)
        print('Difference: ', before - after)

    # Update scenario data
    df = update_scen_data(df, opts)

    #  Write to FS
    df.to_csv('{}/data.csv'.format(data_dir))
    json.dump(info, open('{}/info.json'.format(data_dir), 'w'))
    print('Dataset generation took : {}s'.format(time() - t0))
    DF_CACHED, INFO_CACHED = df, info

    global errors
    print('Errors: {}'.format(Counter(errors)))

    return df, info


def filter_df(df: pd.DataFrame):
    """Filter DT50 value by conditions"""
    def cond(row: int):
        try:
            comment = df.iloc[row]['halflife'].split(';')[1].split(';')[
                0].upper()
            conditions = [c in comment for c in ['<', '>', '~', 'DFOP', 'FOMC']]
            return not any(conditions)
        except Exception as e:
            return True

    drop_rows = []
    for i, row in df.iterrows():
        if not cond(i):
            drop_rows.append(i)

    return df.drop(df.index[drop_rows])


def fix_acidity(df: pd.DataFrame,
                info: dict = None) \
        -> Tuple[pd.DataFrame, dict]:
    """
    Fixes acidity values
    :param df: dataframe 
    :param info: info 
    :return: dataframe
    """
    if info is None:
        info = {}

    def f(s):
        if '-' in str(s):
            return s.split()[0].strip()
        return s

    df['acidity'] = df['acidity'].apply(func=f)

    return df, info


def fix_humidity(df: pd.DataFrame,
                 info: dict = None) \
        -> Tuple[pd.DataFrame, dict]:
    """
    Fixes humidity values bigger than 100%
    :param df: dataframe 
    :param info: info 
    :return: dataframe
    """
    if info is None:
        info = {}

    global errors

    def f(s):
        if s != '?' and (float(s) > 100 or float(s) < 0):
            errors['humidity'] += 1
            return '?'
        return s

    df['humidity'] = df['humidity'].apply(func=f)

    return df, info


def fix_soil_organic_content(df: pd.DataFrame,
                             info: dict = None) \
        -> Tuple[pd.DataFrame, dict]:
    """
    Fixes omcontent values bigger than 10g
    :param df: dataframe 
    :param info: info 
    :return: dataframe
    """
    if info is None:
        info = {}
    global errors

    def f(s):
        if s != '?' and (float(s) > 10 or float(s) < 0):
            errors['omcontent'] += 1
            return '?'
        return s

    df['omcontent'] = df['omcontent'].apply(func=f)

    return df, info


def add_exp_moisture_content(df: pd.DataFrame,
                             info: dict = None) \
        -> Tuple[pd.DataFrame, dict]:
    """Add experimental moisture content"""
    if info is None:
        info = {}

    # Multiply humidity and wsc as proposed in eawag soil paper
    df['exp_moisture_content'] = df['humidity'].astype(float) \
                                 * df['waterstoragecapacity'].astype(float)

    return df, info


def add_biomass_diff(df: pd.DataFrame,
                     info: dict = None) \
        -> Tuple[pd.DataFrame, dict]:
    """Add biomass difference"""
    if info is None:
        info = {}

    # Calculate the absolute biosmass difference by end - start
    df['biomassDiff'] = df['biomassEnd'].astype(float) - \
                        df['biomassStart'].astype(float)

    return df, info


def transform_soil_texture_2(df: pd.DataFrame,
                             info: dict = None) \
        -> Tuple[pd.DataFrame, dict]:
    """
    Corrects soil texture 2
    :param df: dataframe
    :param info: info
    :return: dataframe
    """
    if info is None:
        info = {}

    soil2 = df['soiltexture2'].tolist()
    soil2_index = df.columns.get_loc('soiltexture2')
    del df['soiltexture2']

    sand_column = np.full(shape=len(soil2), fill_value='?', dtype='<U256')
    silt_column = np.full(shape=len(soil2), fill_value='?', dtype='<U256')
    clay_column = np.full(shape=len(soil2), fill_value='?', dtype='<U256')

    df.insert(loc=soil2_index, column='soiltexture2_sand', value=sand_column)
    df.insert(loc=soil2_index + 1, column='soiltexture2_silt',
              value=silt_column)
    df.insert(loc=soil2_index + 2, column='soiltexture2_clay',
              value=clay_column)

    for i, s in enumerate(soil2):
        if '?' in s:
            continue

        splt = s.split('%')
        sand_part = splt[0].split()
        silt_part = splt[1].split()
        clay_part = splt[2].split()

        sand = sand_part[- 1]
        silt = silt_part[- 1]
        clay = clay_part[- 1]

        # Set NaNs to missing values
        if 'nan' in sand.lower() or float(sand) < 0:
            sand = '?'

        if 'nan' in silt.lower() or float(silt) < 0:
            silt = '?'

        if 'nan' in clay.lower() or float(clay) < 0:
            clay = '?'

        # Check if > 105% invalid input
        if sand != '?' and silt != '?' and clay != '?':
            sum_soil = float(sand) + float(silt) + float(clay)
            if sum_soil > 101:
                sand = '?'
                silt = '?'
                clay = '?'
                global errors
                errors['soil'] += 1

        df.loc[i, 'soiltexture2_sand'] = sand
        df.loc[i, 'soiltexture2_silt'] = silt
        df.loc[i, 'soiltexture2_clay'] = clay

    return df, info


def transform_smiles_to_fp(smiles: list,
                           info: dict = None,
                           fptype: str = 'maccs') \
        -> Tuple[np.ndarray, dict]:
    """
    Transforms the 'compound-smiles' column in the dataframe into multiple
    fingerprint-bits columns
    :param smiles: list of smiles
    :param info: info 
    :return: encoded smiles into fingerprints
    """
    if info is None:
        info = {}

    for x in smiles:
        try:
            pybel.readstring('smi', x)
        except Exception as e:
            print(x)

    mols = [pybel.readstring('smi', x) for x in
            smiles]  # Create a list of molecules

    fps = [x.calcfp(fptype=fptype).bits for x in mols]

    # Map ecfps to lower indexing
    if 'ecfp' in fptype:
        # Get all fingerprints
        all_fps = list(set([x for fplist in fps for x in fplist]))
        size = len(all_fps)

        # Map fingerprints to their index in the all_fps list
        fp_map = {all_fps[i]: i for i in range(size)}
        m = lambda x: fp_map[x]
        m2 = lambda x: list(map(m, x))
        fps = list(map(m2, fps))

    rows = []
    cols = []
    data = []
    for i, fplist in enumerate(fps):
        for fp in fplist:
            rows.append(i)
            cols.append(fp)
            data.append(1)
    fp_arr = csr_matrix((data, (rows, cols)))

    return fp_arr, info

def update_scen_data(df: pd.DataFrame, opts: argparse.Namespace = None):
    """Update scenario data from diogo"""
    updates = pd.read_csv('data/scenario_updates.csv', sep=',', header=0)

    scen_map = pd.read_csv('data/scenmap.csv', header=0)
    scen_map['s1'] = scen_map['s1'].apply(func=lambda s: s.split('/')[-1])
    scen_map['s2'] = scen_map['s2'].apply(func=lambda s: s.split('/')[-1])

    d = {}
    for r in scen_map.iterrows():
        r = r[1]
        d[r['s2']] = r['s1']


    df['scen-id'] = df['scenario-id'].apply(func=lambda s: s.split('/')[
        -1].strip().lower())
    updates['scen-id'] = updates['Scenario'].apply(func=lambda s: s.split(
        ':')[1].strip().lower())

    updates['scen-id'] = updates['scen-id'].apply(func=lambda s: d.get(s, s))
    df['smiles'] = df['compound-smiles']
    updates['smiles'] = updates['SMILES']
    df['moisture'] = pd.Series()


    for row in df.iterrows():
        idx = row[0]
        row = row[1]
        id = row['scen-id']
        smiles = row['smiles']
        l = updates.loc[(updates['scen-id'] == id) & (updates['smiles'] == smiles)]
        if l.shape[0] > 0 :
            ur = l.iloc[0] # update_row

            df.loc[idx, ('dt50')] = ur['dt50']
            df.loc[idx, ('clay')] = ur['Clay New']
            df.loc[idx, ('silt')] = ur['Silt New']
            df.loc[idx, ('soiltexture1')] = ur['SoilTexture1']
            df.loc[idx, ('acidity')] = ur['pH CaCl2 New']
            df.loc[idx, ('temperature')] = ur['Temperature New']
            df.loc[idx, ('waterstoragecapacity')] = ur['WSC New']
            df.loc[idx, ('omcontent')] = ur['OrgCont New']
            df.loc[idx, ('cec')] = ur['CEC New']
            df.loc[idx, ('biomassStart')] = ur['log(BioStart) New']
            df.loc[idx, ('biomassEnd')] = ur['log(BioEnd) New']
            df.loc[idx, ('moisture')] = ur['Moisture New']

    del df['scen-id']
    del df['smiles']
    return df

def load_scen_data_encoded(data_dir: str,
                           cache: bool = True,
                           opts: argparse.Namespace = None,
                           ret_type: str = 'np') -> \
        Tuple[Union[np.ndarray, pd.DataFrame], Union[np.ndarray, pd.DataFrame]]:
    """Load encoded scenario data"""
    df, info = load_dataframe(data_dir, cache, opts)


    # Grab scenario features
    df = df.loc[:, 'acidity':]

    # Delete unnecessary data
    del df['compound-id']
    del df['scenario-id']
    del df['scenario-name']
    del df['soilsource']
    del df['halflife']
    del df['spike']  # How about this one?
    # del df['compound-name']
    del df['compound-smiles']

    categorical_columns = [
        'redox',
        'soilclassificationsystem',
        'soiltexture1',
        'scenario-review-status',
        'scenario-type',
        # 'spike'
    ]

    categorical_columns = list(filter(lambda x: x in df.columns.values,
                                      categorical_columns))

    # One hot encode categorical features
    df_encoded = pd.get_dummies(df,
                                columns=categorical_columns,
                                prefix=categorical_columns,
                                prefix_sep='_')

    X = df_encoded
    y = df_encoded['dt50']
    del df_encoded['dt50']

    if ret_type == 'np':
        return X.as_matrix().astype(np.float), y.as_matrix().astype(np.float)
    elif ret_type == 'pd':
        # return X.astype(np.float), y.astype(np.float)
        return X, y
    else:
        raise Exception('Return type "{}" not supported'.format(ret_type))


def load_data_encoded(data_dir: str,
                      cache: bool = True,
                      opts: argparse.Namespace = None) \
        -> Tuple[np.ndarray, np.ndarray]:
    """
    Loads the whole data with one-hot-encoding applied on all categorical
    features
    :return: csr_matrix, list
    """
    X_fp, y = load_fp_data(data_dir, cache, opts=opts)
    X_scen, y = load_scen_data_encoded(data_dir, cache, opts=opts)

    X = np.hstack((X_scen, X_fp))
    return X, y


def load_fp_data(data_dir: str,
                 cache: bool = True,
                 opts: argparse.Namespace = None,
                 ret_type: str = 'np') \
        -> Tuple[
            Union[np.ndarray, pd.DataFrame], Union[np.ndarray, pd.DataFrame]]:
    """
    Load the fingerprint data
    :param data_dir: data directory
    :param cache: cache flag
    :param opts: options
    :return: X, y 
    """
    df, info = load_dataframe(data_dir, cache, opts)
    y = df['dt50']
    f = '{}/data_fp_sparse_{}.npz'.format(data_dir, opts.fptype)
    smiles = df['compound-smiles'].tolist()

    if cache:
        X_fp = scipy.sparse.load_npz(f)
    else:
        X_fp, info = transform_smiles_to_fp(smiles=smiles, fptype=opts.fptype)
        scipy.sparse.save_npz(f, X_fp)

    if ret_type == 'np':
        return X_fp.toarray().astype(np.float), y.as_matrix().astype(np.float)
    else:
        return pd.DataFrame(X_fp.toarray()), pd.DataFrame(y)


def load_comp_descs(opts: argparse.Namespace, ret_type: str = 'np') \
        -> Tuple[
            Union[np.ndarray, pd.DataFrame],
            Union[np.ndarray, pd.DataFrame]
        ]:
    """
    Load the compound descriptor data (not fingerprints)
    :param opts: options
    :return: X, y
    """
    df, info = load_dataframe(opts.data_dir, opts.cache, opts)
    y = df['dt50']

    f = '{}/comp_descs.csv'.format(opts.data_dir)
    if opts.cache and os.path.isfile(f):
        X = pd.read_csv(f)
        if ret_type == 'np':
            return X.as_matrix().astype(np.float), y.as_matrix().astype(
                np.float)
        else:
            return X, y

    smiles = df['compound-smiles'].tolist()
    mols = [pybel.readstring('smi', x) for x in smiles]
    descs = [x.calcdesc(pybel.descs) for x in mols]

    descs = pd.DataFrame(descs)
    # Drop empty descriptors ( all are nan )
    del descs['InChI']
    del descs['InChIKey']
    del descs['L5']
    del descs['cansmi']
    del descs['cansmiNS']
    del descs['formula']
    del descs['s']
    del descs['smarts']
    del descs['title']

    X = descs
    X.to_csv(f)

    if ret_type == 'np':
        return X.as_matrix().astype(np.float), y.as_matrix().astype(np.float)
    else:
        return X, y


def load_reactions(opts: argparse.Namespace):
    """Load compound reactions"""
    data_dir = opts.data_dir
    reactions_file = '{}/out.json'.format(data_dir)
    f = open(reactions_file, 'r')
    reactions_str = '\n'.join(f.readlines())

    compounds = json.loads(reactions_str)['data']
    rules = set()

    # Fix structure
    for comp in compounds:
        rls = dict()
        for rule in comp['rules']:
            for key in rule:
                rls[key] = rule[key]
                rules.add(key)
        comp['rules'] = rls

    num_comps = len(compounds)
    num_rules = len(rules)
    shape = (num_comps, num_rules)
    X = np.ndarray(shape, dtype=bool)

    smiles = []
    rules_sorted = sorted(rules)
    for idx_comp, comp in enumerate(compounds):
        smiles.append(comp['smiles'])
        rls = comp['rules']
        for idx_rule, rl in enumerate(rules_sorted):
            X[idx_comp, idx_rule] = rls[rl]

    df_rules = pd.DataFrame(data=X, columns=rules_sorted)
    df_rules = df_rules.assign(smiles=pd.Series(smiles))

    df, info = load_dataframe(opts.data_dir, opts.cache, opts)

    merged = df.merge(df_rules, left_on='compound-smiles', right_on='smiles')
    X = merged[rules_sorted]
    y = merged['dt50']
    return X.as_matrix().astype(np.float), y.as_matrix().astype(np.float)


def ensure_dir(f: str) -> None:
    """
    Ensures that a given directory exists
    :param f: directory path
    :return:
    """
    d = os.path.dirname(f)
    if not os.path.exists(d):
        os.makedirs(d)


def make_groups(X_fp: np.ndarray, X_scen: np.ndarray):
    """Creates groups for cross validation based on compounds and scenarios"""

    def update_group(d1: dict, d2: dict, old_group: int, new_group: int):
        for entry, group in d1.items():
            if group == old_group:
                d1[entry] = new_group
                update_group(d2, d1, old_group, new_group)

    sys.setrecursionlimit(10000)
    cg = dict()
    sg = dict()

    comps = [hashlib.sha256(x.tostring()).digest() for x in X_fp]
    scens = [hashlib.sha256(x.tostring()).digest() for x in X_scen]

    i = 0
    for c, s in zip(comps, scens):
        # Create new group
        if c not in cg.keys() and s not in sg.keys():
            i += 1
            cg[c] = i
            sg[s] = i

        # Set s group to c group
        elif c in cg.keys() and s not in sg.keys():
            sg[s] = cg[c]

        # Set c group to s group
        elif c not in cg.keys() and s in sg.keys():
            cg[c] = sg[s]

        # Update groups if different
        elif c in cg.keys() and s in sg.keys() \
                and cg[c] != sg[s]:
            i += 1
            update_group(cg, sg, cg[c], i)
            update_group(cg, sg, sg[s], i)

    comp_groups = [cg[c] for c in comps]
    scen_groups = [sg[s] for s in scens]

    count = 0
    for c, s in zip(comp_groups, scen_groups):
        if c != s:
            count += 1
    if count > 0:
        raise Exception('Grouping failed. Count = {}'.format(count))

    return np.array(comp_groups)


def save_opts(prefix, opts):
    """Save options"""
    if prefix[-1] != '/':
        prefix += '/'
    with open(prefix + 'options.txt', 'w') as f:
        f.write('Options: \n' + '\n'.join(['{} = {} '.format(
            x, y) for x, y in sorted(vars(opts).items())]))


def filter_by_num_scens(X_fp: np.ndarray,
                        X_scen: np.ndarray,
                        y: Union[List, np.ndarray],
                        n: int) \
        -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """Filter compounds by minimum number of scenarios"""
    comps = [hashlib.md5(x.tostring()).digest() for x in X_fp]
    scens = [hashlib.md5(x.tostring()).digest() for x in X_scen]
    comp_dict = {}
    for comp, scen, hl in zip(comps, scens, y):
        if comp in comp_dict:
            comp_dict[comp] += [(scen, hl)]
        else:
            comp_dict[comp] = [(scen, hl)]

    keep_comps = set()
    for key, val in comp_dict.items():
        if len(val) >= n:
            keep_comps.add(key)

    mask = [hashlib.md5(x.tostring()).digest() in keep_comps for x in X_fp]
    return X_fp[mask], X_scen[mask], np.array(y)[mask]


class KNNImputer(BaseEstimator, TransformerMixin):
    """KNN Imputation"""
    def __init__(self, k: int, fp_dim: int):
        from fancyimpute import KNN
        self.k = k
        self.knn = KNN(k=k)
        self.fp_dim = fp_dim

    def fit(self, X, y=None):
        self.X_scen_train = X[:, self.fp_dim:]
        return self

    def transform(self, X, y=None):
        if not np.isnan(X).any():
            # Not missing any values
            return X

        # Get scenario features
        X_scen = X[:, self.fp_dim:]

        # Check if this was the training set
        eq = X_scen.shape == self.X_scen_train.shape
        if eq:
            X_miss_val = X_scen
        else:
            X_miss_val = np.vstack((self.X_scen_train, X_scen))

        # Impute

        X_complete = self.knn.complete(X_miss_val)

        if eq:
            X_trans = X_complete
        else:
            split = self.X_scen_train.shape[0]
            X_trans = X_complete[split:]

        X_fp = X[:, :self.fp_dim]
        X_final = np.hstack((X_fp, X_trans))
        return X_final

    def fit_transform(self, X, y=None, **fit_params):
        return self.fit(X).transform(X)

    def set_params(self, **params):
        for parameter, value in params.items():
            self.__setattr__(parameter, value)
        return self

    def get_params(self, deep=True):
        params = {'k': self.k, 'fp_dim': self.fp_dim}
        return params


class ScenarioDistanceTransformer(BaseEstimator, TransformerMixin):
    """Transformer for scenario distances"""
    def __init__(self, fp_dim: int):
        self.fp_dim = fp_dim

    def fit(self, X, y=None):
        self.X_scen_train = X[:, self.fp_dim:]
        return self

    def transform(self, X, y=None):
        dists = []

        # Get scenario features
        X_scen = X[:, self.fp_dim:]

        for x in X_scen:
            d = np.linalg.norm(x - self.X_scen_train, axis=0)
            dists.append(d)

        X_dists = np.array(dists)
        X_fp = X[:, :self.fp_dim]
        X_final = np.hstack((X_fp, X_dists))
        return X_final

    def fit_transform(self, X, y=None, **fit_params):
        return self.fit(X).transform(X)

    def set_params(self, **params):
        for parameter, value in params.items():
            self.__setattr__(parameter, value)
        return self

    def get_params(self, deep=True):
        params = {'fp_dim': self.fp_dim}
        return params


class ScenarioScaler(StandardScaler):
    """Scaler which only scales on scenarios and not on compound SMILES bits"""
    def __init__(self, fp_dim):
        super().__init__()
        self.fp_dim = fp_dim
        pass

    def fit(self, X, y=None):
        X_scen = X[:, self.fp_dim:]
        return super().fit(X_scen, y)

    def transform(self, X, y=None, copy=None):
        X_fp = X[:, :self.fp_dim]
        X_scen = X[:, self.fp_dim:]

        X_scen_trans = super().transform(X_scen, y)

        return np.hstack((X_fp, X_scen_trans))

    def set_params(self, **params):
        for parameter, value in params.items():
            self.__setattr__(parameter, value)
        return self

    def get_params(self, deep=True):
        params = {'fp_dim': self.fp_dim}
        sup_params = super().get_params()
        params.update(sup_params)
        return params


class CustomImputer(BaseEstimator, TransformerMixin):
    """Custom Imputer"""
    def __init__(self, fp_dim, type):
        self.type = type
        self.fp_dim = fp_dim
        self.imputer = {
            'KNN': KNN(),
            'MICE': MICE(),
            'SOFT': SoftImpute(),
            'MF': MatrixFactorization(),
            'MEAN': Imputer()}.get(self.type)

    def fit(self, X, y=None):
        self.X_scen_train = X[:, self.fp_dim:]
        if self.type == 'MEAN':
            self.imputer.fit(X)
        return self

    def transform(self, X, y=None):
        if self.type == 'MEAN':
            return self.imputer.transform(X)

        # Get scenario features
        X_scen_test = X[:, self.fp_dim:]

        # Stack train and test
        stacked = np.vstack((self.X_scen_train, X_scen_test))

        # Impute on all information
        filled = self.imputer.complete(stacked)

        # Get only test set
        filled_test = filled[self.X_scen_train.shape[0]:]

        # Stack with compound features
        X_fp = X[:, :self.fp_dim]
        X_final = np.hstack((X_fp, filled_test))
        return X_final

    def fit_transform(self, X, y=None, **fit_params):
        return self.fit(X).transform(X)

    def set_params(self, **params):
        for parameter, value in params.items():
            self.__setattr__(parameter, value)
        return self

    def get_params(self, deep=True):
        params = {'fp_dim': self.fp_dim, 'type': self.type}
        return params
