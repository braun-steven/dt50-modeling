from enum import Enum


class Feature(Enum):
    FPS = 0
    SCENS = 1
    RULES = 2

    def __str__(self):
        if self == Feature.FPS:
            return "fps"
        elif self == Feature.SCENS:
            return "scens"
        elif self == Feature.RULES:
            return "rules"


def feature_to_str(feat):
    """
    Turns a list of features or a feature into a string
    :param feat: may be a list or a single feature
    :return: string repr
    """
    if type(feat) == list:
        return str([str(f) for f in feat])
    elif type(feat) == Feature:
        return str(feat)