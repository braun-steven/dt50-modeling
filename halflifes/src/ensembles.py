from time import time

import numpy as np
from sklearn.base import BaseEstimator, RegressorMixin, ClusterMixin, clone
from sklearn.base import ClassifierMixin
from sklearn.cluster import KMeans, DBSCAN, MeanShift
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, \
    GradientBoostingClassifier, RandomForestRegressor
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import Imputer, StandardScaler
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from typing import Any, Union, Iterable, Tuple, SupportsFloat, List


class ClusterEnsemble(BaseEstimator, RegressorMixin):
    def __init__(self,
                 fp_dim: int = 166,
                 k: int = 3,
                 base_estimator=RandomForestRegressor(n_estimators=100)):
        self._fp_dim = fp_dim
        self._k = k
        self._base_estimator = base_estimator
        self._base_clusterer = KMeans(n_clusters=k)

    def set_params(self, **params):
        for parameter, value in params.items():
            self.__setattr__(parameter, value)
        return self

    def get_params(self, deep=True):
        params = {'fp_dim': self._fp_dim, 'k': self._k,
                  'base_estimator': self._base_estimator}
        return params

    def fit(self, X, y):
        X_fp = X[:, :self._fp_dim]
        self._base_clusterer.fit(X_fp)

        labels = self._base_clusterer.labels_
        clusters = dict()
        for idx, l in enumerate(labels):
            e_data = X[idx]
            e_target = y[idx]
            if l in clusters.keys():
                clusters[l].append((e_data, e_target))
            else:
                clusters[l] = [(e_data, e_target)]

        self.estimators = dict()
        for l in clusters.keys():
            data = [t[0] for t in clusters[l]]
            target = [t[1] for t in clusters[l]]
            est = clone(self._base_estimator)
            est.fit(data, target)
            self.estimators[l] = est

    def predict(self, X):
        X_fp = X[:, :self._fp_dim]

        res = []
        for idx, x in enumerate(X_fp):
            l = self._base_clusterer.predict(x.reshape(1, -1))[0]
            el = X[idx]
            y_pred = self.estimators[l].predict(el.reshape(1, -1))
            res.append(y_pred)

        return np.array(res)


class TempEstimator(BaseEstimator, RegressorMixin):
    def __init__(self,
                 temp_idx,
                 base1=RandomForestRegressor(n_estimators=100),
                 base2=RandomForestRegressor(n_estimators=100)):
        self.temp_idx = temp_idx

        self.base1 = base1
        self.base2 = base2
        self.pipe1 = Pipeline(
            steps=[
                ('imputer', Imputer(strategy='mean')),
                ('scaler', StandardScaler()),
                ('reg', base1)])
        self.pipe2 = Pipeline(
            steps=[
                ('imputer', Imputer(strategy='mean')),
                ('scaler', StandardScaler()),
                ('reg', base2)])

    def set_params(self, **params):
        for parameter, value in params.items():
            self.__setattr__(parameter, value)
        return self

    def get_params(self, deep=True):
        params = {'temp_idx': self.temp_idx,
                  'base1': self.base1,
                  'base2': self.base2}
        return params

    def fit(self, X, y):

        X_temp_pos = []
        y_temp_pos = []
        X_temp_neg = []
        y_temp_neg = []

        for idx, x in enumerate(X):
            if x[self.temp_idx] > 19.9 or x[self.temp_idx] < 20.1:
                X_temp_pos.append(x)
                y_temp_pos.append(y[idx])
            else:
                X_temp_neg.append(x)
                y_temp_neg.append(y[idx])

        self.pipe1.fit(X_temp_pos, y_temp_pos)
        self.pipe2.fit(X_temp_neg, y_temp_neg)

    def predict(self, X):
        res = []

        for x in X:
            if x[self.temp_idx] > 19.9 or x[self.temp_idx] < 20.1:
                res.append(self.pipe1.predict(x))
            else:
                res.append(self.pipe2.predict(x))

        return np.array(res)


class EOCClassifier(BaseEstimator, ClassifierMixin):
    """Votes over classifier confidences between naive bayes, random
    forest, SGD, logistic regression and SVM with an rbf kernel."""

    def __init__(self, n_jobs=1, aggregate='weighted'):
        # type: (int, str) -> None
        """
        Init the classifier.
        :param n_jobs: Number of parallel jobs if possible.
        :param aggregate: How to aggregate over the probabilities of the
        single classifiers.
        """
        self.aggregate = aggregate
        self.n_jobs = n_jobs
        self._init_clfs()

    def _init_clfs(self):
        # type: () -> None
        """
        Initializes the classifiers
        :return:
        """

        ada = AdaBoostClassifier(base_estimator=DecisionTreeClassifier(),
                                 learning_rate=0.1,
                                 algorithm='SAMME.R')

        gb = GradientBoostingClassifier(n_estimators=1000,
                                        learning_rate=0.1)

        sgd = SGDClassifier(penalty='l2',
                            loss='log',
                            average=True,
                            alpha=10 ** -7,
                            n_jobs=self.n_jobs)

        rf = RandomForestClassifier(n_estimators=200,
                                    class_weight='balanced',
                                    max_features='log2',
                                    n_jobs=self.n_jobs)

        svm = SVC(C=8,
                  kernel='rbf',
                  gamma=0.03125,
                  probability=True)

        self.clfs = [
            gb,
            sgd,
            rf,
            svm,
            ada
        ]

    def set_params(self, **params):
        return super(EOCClassifier, self).set_params(**params)

    def get_params(self, deep=True):
        return super(EOCClassifier, self).get_params(deep)

    def fit(self, X, y):
        # type: (np.ndarray[Union[int, float]], List[int]) -> EOCClassifier
        """
        For the classifier on the training data
        :param X: Training instances
        :param y: Training labels
        :return: self
        """
        for clf in self.clfs:
            t0 = time()
            clf.fit(X, y)

        # Get GridSearchCV best_estimators
        best_clfs = []
        for clf in self.clfs:
            if isinstance(clf, GridSearchCV):
                best_clfs.append(clf.best_estimator_)
            else:
                best_clfs.append(clf)

        self.clfs = best_clfs
        return self

    def _aggregrate_probabilites(selfs, tuples, method='weighted'):
        # type: (np.ndarray[float], str) -> List[float]
        """
        Decides how to aggregate over the probability predictions from the
        different classifiers
        :param tuples: List of probability distributions for the classes
        given by each of the classifier
        :param method: Can be 'max' or 'weighted'
        :return: Aggregated probability distribution
        """

        best_tup = [0.0, 0.0]

        if method == 'max':
            # Find the highest probability
            for tup in tuples:
                if max(tup) > max(best_tup):
                    best_tup = tup
        elif method == 'weighted':
            # Weight over all probabilities
            for i in range(tuples.shape[1]):
                best_tup[i] = np.sum(tuples[:, i]) / float(len(tuples))

        return best_tup

    def predict(self, X):
        # type: (np.ndarray[Union[int, float]]) -> np.ndarray[int]
        """
        Predicts the labels for all instances in X
        :param X: List of input instances
        :return: List of labels, shape: (len(X), 1)
        """
        probas = self.predict_proba(X)
        res = []
        for p0, p1 in probas:
            if p0 > p1:
                res.append(0)
            else:
                res.append(1)

        return np.array(res)

    def predict_proba(self, X):
        # type: (np.ndarray[Union[int, float]]) -> np.ndarray[List[float]]
        """
        Predicts the probabilities for all classes of all instances in X
        :param X: List of input instances
        :return: List of probabilities, shape: (len(X), num_classes)
        """
        # probas = []
        # for clf in self.clfs:
        #     probas.append(clf.predict_proba(X))

        probas = np.array([clf.predict_proba(X) for clf in self.clfs])

        res = []

        for i in range(0, probas.shape[1]):
            res.append(self._aggregrate_probabilites(tuples=probas[:, i],
                                                     method=self.aggregate))

        return np.array(res)
