#!../env/bin/python

import argparse
from time import strftime, gmtime

import matplotlib
import pandas as pd
from scipy.stats import skew
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.model_selection import GridSearchCV, GroupKFold, train_test_split, \
    GroupShuffleSplit
from sklearn.pipeline import Pipeline
from sklearn.utils import shuffle

from Utils.model_util import get_reg, Result, ResultSet
from Utils.data_utils import load_fp_data, load_scen_data_encoded, \
    make_groups, save_opts,\
    filter_by_num_scens
from Utils.args import parse_arg

matplotlib.use('Agg')
import numpy as np

import matplotlib.pyplot as plt

plt.style.use('ggplot')
from sklearn import metrics
from sklearn.preprocessing import Imputer, StandardScaler
from sklearn import model_selection


def replace_indicator_variables(X_scen: pd.DataFrame):
    """Replace scenario variables by indicator variables"""
    for col in X_scen.columns:
        c = X_scen[col]
        del X_scen[col]
        mean = c.mean()
        for f in [0.1, 0.25, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 4.0, 5.0, 8.0]:
            # for f in [1.0, 2.0]:
            X_scen['{}_{}_std'.format(col, f)] = (c - mean).abs() > f * c.std()


def build_regression(opts, X_fp, X_scen, y, save=True):
    """Build the regression model and return scores"""
    # Copy the datasets
    X_fp = X_fp.copy()
    X_scen = X_scen.copy()
    y = np.array(y).copy()

    if type(X_fp) is pd.DataFrame:
        X_fp = X_fp.as_matrix()

    if type(X_scen) is pd.DataFrame:
        X_scen = X_scen.as_matrix()

    if type(y) is pd.Series:
        y = y.as_matrix()

    # Use log scale for better evaluation of metrics
    y = np.log(y)
    y[y == -np.inf] = y[y != -np.inf].min()

    # Drop values lower than 1 day and higher than 320 days
    if opts.filter_dt50:
        print('Dropping dt50 not in 1 < x < 320')
        below_1 = y[y < np.log(1)].shape[0]
        print('Number of dt50 < 1: ', below_1)
        above_320 = y[y > np.log(320)].shape[0]
        print('Number of dt50 > 320: ', above_320)
        print('Dropping a total of: ', below_1 + above_320)
        mask = np.logical_and(y > np.log(1), y < np.log(320))
        y = y[mask]
        X_fp = X_fp[mask]
        X_scen = X_scen[mask]

    # Filter by percentiles
    if opts.filter_dt50_perc:
        p = opts.filter_dt50_perc
        print('Filtering by percentiles: ', p)
        y_idxed = [(i, yp) for i, yp in enumerate(y)]
        y_sorted = np.array((sorted(y_idxed, key=lambda t: t[1])))
        cutoff = int(y.shape[0] * p)
        print('Cutoff: ', cutoff)
        y_sorted_cut = y_sorted[cutoff: (-1) * cutoff]
        print('Min days: ', np.exp(y_sorted_cut[0][1]))
        print('Max days: ', np.exp(y_sorted_cut[-1][1]))
        mask = y_sorted_cut[:, 0].astype(int)
        y = y[mask]
        X_fp = X_fp[mask]
        X_scen = X_scen[mask]

    if opts.min_num_scen > 0:
        n = opts.min_num_scen
        X_fp, X_scen, y = filter_by_num_scens(X_fp, X_scen, y, n)

    print(f'N = {X_fp.shape[0]}')

    # Use only some small test examples in debug mode
    if opts.debug:
        # opts.regs = ['dt']
        X_fp, X_scen = X_fp[0:150], X_scen[0:150]
        y = y[0:150]

    num_feats = X_fp.shape[1] + X_scen.shape[1]
    # Define regs to be evaluated
    regs = [(tp, get_reg(tp, opts, num_feats)) for tp in opts.regs]

    def RMSE(y_true, y_pred):
        return np.sqrt(metrics.mean_squared_error(y_true, y_pred))

    scorings = [
        ('RMSE', RMSE),
        # ('MSE', metrics.mean_squared_error),
        # ('MAE', metrics.mean_absolute_error),
        # ('MedAE', metrics.median_absolute_error),
        ('R2', metrics.r2_score),
        # ('ExplVar', metrics.explained_variance_score)
    ]

    CURRENT_TIME_STR = strftime("%Y-%m-%d_%H-%M-%S", gmtime())
    prefix = 'run/' + CURRENT_TIME_STR + '_'
    if opts.tag:
        prefix += opts.tag
    else:
        prefix += 'reg'
    results = ResultSet(out_dir=prefix,
                        metrics=scorings)


    for tp, reg in regs:
        print('Fitting: ' + reg.__class__.__name__)
        pipe = Pipeline(
            steps=[
                # ('imputer', CustomImputer(fp_dim=fp_dim, type=opts.imputer)),
                ('imputer', Imputer(strategy='mean')),
                ('scaler', StandardScaler()),
                # ('distmapper', ScenarioDistanceTransformer(fp_dim=fp_dim)),
                # ('net', HLNetwork(opts, fp_dim, comp_desc_dim)),
                ('reg', reg)])
        r = validate_regression(pipe, X_fp, X_scen, y, opts, tp)
        results.add(r)

    print(results.table())
    if save:
        results.save()
        save_opts(prefix, opts)

    return (r.apply(r2_score), r.apply(mean_squared_error))


def validate_regression(pipe: Pipeline,
                        X_fp: np.ndarray,
                        X_scen: np.ndarray,
                        y: list,
                        opts: argparse.Namespace,
                        name: str) -> Result:
    """
    Validate a given model against different metrics
    :param pipe: Model
    :param X: Input data
    :param y: halflifes
    :return: None
    """
    # X = np.hstack((X_fp, X_comp_desc, X_scen))
    X = np.hstack((X_fp, X_scen))

    # Remove skewness
    # log transform skewed numeric features:
    # X = fix_skewness(X, opts)

    reg = pipe.named_steps['reg']
    if isinstance(reg, GridSearchCV):
        ### Grid-Search ###
        # Evaluate the best parameters on a train set and validate on the
        # test set

        groups = make_groups(X_fp, X_scen)
        rs = model_selection.GroupShuffleSplit(n_splits=1,
                                               test_size=opts.test_size,
                                               random_state=42)
        [(train, test)] = rs.split(X, y, groups)
        X_train = X[train]
        X_test = X[test]
        y_train = y[train]
        y_test = y[test]
        groups_train = groups[train]

        pipe.fit(X_train, y_train, reg__groups=groups_train)
        print(reg.__class__.__name__ + ' BEST PARAMS: ')
        print(reg.best_params_)

        best_reg = reg.best_estimator_
        print('Best estimator:')
        print(best_reg)
        pipe.set_params(reg=best_reg)

        y_pred = pipe.predict(X_test)

        result = Result(name, y_test, y_pred, opts=opts)
    else:
        # Just do simple holdout for performance reason
        if opts.cv == 0:
            # Single run with one train and one test set
            rs = model_selection.GroupShuffleSplit(n_splits=1,
                                                   test_size=opts.test_size,
                                                   random_state=42)

            # Create groups from hashes of the rows
            groups = make_groups(X_fp, X_scen)
            X, y, groups = shuffle(X, y, groups, random_state=42)
            [(train, test)] = rs.split(X, y, groups)
            X_train, X_test, y_train, y_test = X[train], X[test], y[train], y[
                test]
            pipe.fit(X_train, y_train)
            y_pred = pipe.predict(X_test)
            y = y_test

        else:

            if opts.cv == -1:
                cv = model_selection.LeaveOneOut()
                # cv = model_selection.LeaveOneGroupOut()
            else:
                # cv = GroupKFold(n_splits=opts.cv)
                cv = 10 if X.shape[0] > 10 else X.shape[0]

            # groups = make_groups(X_fp, X_scen)
            y_pred = model_selection.cross_val_predict(estimator=pipe,
                                                       X=X,
                                                       y=y,
                                                       cv=cv,
                                                       # TODO:
                                                       # uncomment for multi
                                                       # compounds
                                                       n_jobs=opts.n_jobs,
                                                       # groups=groups
                                                       )
        result = Result(name, y, y_pred, opts=opts)

    return result


def fix_skewness(X, opts):
    """Fix skewed variables that have a skewness above opts.skew_thres"""
    X = pd.DataFrame(X)
    numeric_feats = X.dtypes[X.dtypes != "object"].index
    skewed_feats = X[numeric_feats].apply(
        lambda x: np.abs(skew(x.dropna())))  # compute skewness
    thres = opts.skew_thres
    mask_upper = skewed_feats > thres
    mask_positive = X[numeric_feats].apply(lambda x: np.all(x >= 0))
    mask = np.logical_and(mask_upper, mask_positive)
    skewed_feats = skewed_feats[mask]
    skewed_feats = skewed_feats.index
    print('Threshold: ', thres)
    print('Num skewed feats: ', np.count_nonzero(skewed_feats))
    X[skewed_feats] = np.log1p(X[skewed_feats])
    X = X.as_matrix()
    return X


def fix_gpu_usage():
    import os
    import tensorflow as tf
    import keras.backend.tensorflow_backend as KTF

    def get_session(gpu_fraction=0.3):
        '''Assume that you have 6GB of GPU memory and want to allocate ~2GB'''

        num_threads = os.environ.get('OMP_NUM_THREADS')
        gpu_options = tf.GPUOptions(
            per_process_gpu_memory_fraction=gpu_fraction)

        if num_threads:
            return tf.Session(config=tf.ConfigProto(
                gpu_options=gpu_options,
                intra_op_parallelism_threads=num_threads))
        else:
            return tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))

    KTF.set_session(get_session())


def reset_opts(opts):
    opts.batch_size = 32
    opts.epochs_dae = 150
    opts.epochs_hl = 100
    opts.num_switches = 10
    opts.lr = 0.01
    opts.kernel_reg = 0.0001
    opts.act_reg = 0
    opts.noise_factor = 0.66
    opts.compression_rate = 0.5
    opts.scen_encoding_dim = 4
    opts.fp_encoding_dim = 32


if __name__ == '__main__':
    # Setup multiprocessing for mogon
    # import multiprocessing as mp
    # mp.set_start_method('forkserver', True)

    # fix_gpu_usage()

    # Get parser arguments
    opts = parse_arg()

    np.random.seed(42)

    data_dir = opts.data_dir
    X_fp, y = load_fp_data(data_dir, opts.cache, opts, ret_type='pd')
    X_scen, y = load_scen_data_encoded(data_dir, opts.cache, opts,
                                       ret_type='pd')
    del X_scen['compound-name']
    if opts.drop_temp:
        # Filter temperature != 20°
        size_before = X_scen.shape[0]
        temp_idx = X_scen[(X_scen['temperature'] < 19.9) | (
            X_scen['temperature'] > 20.1)].index
        X_fp = X_fp.iloc[temp_idx]
        X_scen = X_scen.iloc[temp_idx]
        y = y.iloc[temp_idx]
        size_after = X_scen.shape[0]
        print(f'Dropped {size_before - size_after} for temp.')


    build_regression(opts, X_fp, X_scen, y)

    # X_fp, y = load_fp_data(data_dir, opts.cache, opts, ret_type='pd')
    # X_scen, y = load_scen_data_encoded(data_dir, opts.cache, opts,
    #                                    ret_type='pd')
    # compounds = [
    #     '2,4-D',
    #     'Acetochlor',
    #     'Chlorsulfuron',
    #     'Diuron',
    #     'Florasulam',
    #     'Mandipropamid',
    #     'Metamitron',
    #     'Metazachlor',
    #     'Metribuzin',
    #     'Metsulfuron-methyl',
    #     'Pyroxsulam (XDE-742)',
    #     'Quinmerac',
    #     'Tribenuron-methyl'
    # ]
    #
    # if opts.drop_temp:
    #     # Filter temperature != 20°
    #     size_before = X_scen.shape[0]
    #     temp_idx = X_scen[(X_scen['temperature'] > 19.9) & (
    #         X_scen['temperature'] < 20.1)].index
    #     X_fp = X_fp.iloc[temp_idx]
    #     X_scen = X_scen.iloc[temp_idx]
    #     y = y.iloc[temp_idx]
    #     size_after = X_scen.shape[0]
    #     print(f'Dropped {size_before - size_after} for temp.')
    #     X_fp.reset_index(inplace=True)
    #     X_scen.reset_index(inplace=True)
    #
    # elif opts.drop_temp_inv:
    #     # Filter temperature == 20°
    #     size_before = X_scen.shape[0]
    #     temp_idx = X_scen[(X_scen['temperature'] < 19.9) | (
    #         X_scen['temperature'] > 20.1)].index
    #     X_fp = X_fp.iloc[temp_idx]
    #     X_scen = X_scen.iloc[temp_idx]
    #     y = y.iloc[temp_idx]
    #     size_after = X_scen.shape[0]
    #     print(f'Dropped {size_before - size_after} for temp.')
    #     X_fp.reset_index(inplace=True)
    #     X_scen.reset_index(inplace=True)
    #
    # for c in compounds:
    #     np.random.seed(42)
    #
    #     idx = X_scen.loc[X_scen['compound-name'] == c].index
    #
    #     X_fp_tmp = X_fp.iloc[idx]
    #     X_scen_tmp = X_scen.iloc[idx]
    #     y_tmp = y.iloc[idx]
    #     if X_scen_tmp.shape[0] > 1:
    #         del X_scen_tmp['compound-name']
    #         print('\nCompound: ', c)
    #         build_regression(opts, X_fp_tmp, X_scen_tmp, y_tmp)
    #     else:
    #         print('Could not find enough data for:', c)


