import argparse

from joblib import Parallel, delayed
from sklearn import model_selection, clone
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import Imputer, StandardScaler
from sklearn.utils import shuffle
from typing import List, Tuple

from Utils.data_utils import load_fp_data, load_scen_data_encoded, make_groups
import numpy as np

from main_classification import replace_intervall


def p_value(perm_res: List[Tuple[float, float]], orig_res_mse: float,
            orig_res_r2) \
        -> Tuple[float, float]:
    """
    Permutation based p-value test
    Calculate the fraction of how many randomized datasets have been better 
    than the non-randomized dataset
    :param perm_res: List of evaluateuation results of the permuted datasets
    :param orig_res: Evaluation result of the original dataset
    :return: 
    """
    count_mse = 0
    count_r2 = 0
    for pr in perm_res:
        if pr[0] < orig_res_mse:
            count_mse += 1
        if pr[1] > orig_res_r2:
            count_r2 += 1

    p_value_mse = (count_mse + 1) / float(len(perm_res) + 1)
    p_value_r2 = (count_r2 + 1) / float(len(perm_res) + 1)
    return p_value_mse, p_value_r2


def main():
    opts = parse_arg()

    # Get the data
    data_dir = opts.data_dir
    X_fp, y = load_fp_data(data_dir, opts.cache, opts)
    X_scen, y = load_scen_data_encoded(data_dir, opts.cache, opts)
    X = np.hstack((X_fp, X_scen))

    # Use log scale for better evaluateuation of mse and mae metrics
    y = np.log(y)
    y[y == -np.inf] = y[y != -np.inf].min()

    # Use only some small test examples in debug mode
    if opts.debug:
        X_fp, X_scen = X_fp[0:150], X_scen[0:150]
        X = X[0:150]
        y = y[0:150]

    reg = RandomForestRegressor(n_estimators=50, max_features='sqrt',
                                random_state=42)
    pipe = Pipeline(
        steps=[
            ('imputer', Imputer(strategy='mean')),
            ('scaler', StandardScaler()),
            ('reg', reg)])

    rs = model_selection.GroupKFold(n_splits=opts.cv)
    groups = make_groups(X_fp, X_scen)

    orig_res_mse, orig_res_r2 = evaluate(clone(pipe), X, y, groups, rs)
    print('Orig results mse:', orig_res_mse)
    print('Orig results r2:', orig_res_r2)

    ######
    # Test 1 (Permute labels)
    #####


    # parallel = Parallel(n_jobs=opts.n_jobs, verbose=opts.verbose)
    # test_1_results = parallel(
    #     delayed(evaluate_test1)(clone(pipe), X, y, i, groups, rs)
    #     for i in range(opts.num_tests)
    # )
    #
    # test_1_results = np.array(test_1_results)
    # test_1_p_value = p_value(test_1_results, orig_res_mse, orig_res_r2)
    #
    # print('Test 1 p-value: {}'.format(test_1_p_value))
    #
    # print('Test 1 means:', test_1_results[:, 0].mean(), test_1_results[:,
    #                                                     1].mean())

    #####
    # Test 2 (Permute data columns (per class?))
    #####
    parallel = Parallel(n_jobs=opts.n_jobs, verbose=opts.verbose)

    l = [
        # (0, X.shape[1], 'full'),
        #  (0, X_fp.shape[1], 'compounds'),
         (X_fp.shape[1], X.shape[1], 'scenarios')
    ]
    for start, end, tag in l:
        test_2_results = parallel(
            delayed(evaluate_test2)(clone(pipe), X, y, i, groups, rs, 10,
                                    start, end)
            for i in range(opts.num_tests)
        )
        test_2_results = np.array(test_2_results)
        test_2_p_value = p_value(test_2_results, orig_res_mse, orig_res_r2)
        print('Executed for ', tag)
        print('Test 2 p-value: {}'.format(test_2_p_value))
        print('Test 2 means: {}, {}'
              .format(
                      test_2_results[:, 0].mean(),
                      test_2_results[:, 1].mean()
                      )
              )


def evaluate(estimator, X, y, groups, cv):
    y_pred = model_selection.cross_val_predict(estimator=estimator,
                                               X=X,
                                               y=y,
                                               cv=cv,
                                               n_jobs=1,
                                               groups=groups)
    return mean_squared_error(y, y_pred), r2_score(y, y_pred)


def evaluate_test1(estimator, X, y, i, groups, cv):
    y_perm = shuffle(y, random_state=i)
    return evaluate(estimator, X, y_perm, groups, cv)


def evaluate_test2(estimator, X, y, i, groups, cv, step_size, col_start,
                   col_end):
    ranges = list(np.arange(0, 320, step_size))
    interval = np.log(ranges)
    y_discretized = np.array([replace_intervall(yp, interval) for yp in y])

    X_copy = np.array(X)

    groups_unique = set(groups)

    for g in groups_unique:
        # Get submatrix of group g
        group_idx = groups == g
        X_g = X_copy[group_idx]
        y_disc_groups = y_discretized[group_idx]

        for ival in interval:
            # Get submatrix of class c
            idxes = y_disc_groups == ival
            X_c = X_g[idxes]

            # Shuffle column wise
            for idx in range(col_start, col_end):
                X_c[:, idx] = shuffle(X_c[:, idx], random_state=idx + 10000 * i)

            # Reassign columns to class matrix
            X_g[idxes] = X_c

        # Reassign groups to final matrix
        X_copy[group_idx] = X_g

    return evaluate(estimator, X_copy, y, groups, cv)


def parse_arg() -> argparse.Namespace:
    """Parse commandline arguments
    :return: argument dict
    """
    print('Parsing arguments')
    parser = argparse.ArgumentParser('Halflife prediction')
    parser.add_argument('--data-dir', '-d', default='data', type=str,
                        help='Data directory')
    parser.add_argument('--out', '-o', default='run', type=str,
                        help='Output directory')
    parser.add_argument('--cache', '-c', default=False, action='store_true',
                        help='Use the cache')
    parser.add_argument('--verbose', '-v', default=0, type=int,
                        help='Verbosity level')
    parser.add_argument('--n-jobs', '-n', type=int, default=4,
                        help='Number of parallel jobs')
    parser.add_argument('--cv', '-cv', type=int, default=10,
                        help='Number of cross validations')
    parser.add_argument('--num-tests', '-nt', type=int, default=50,
                        help='Number of tests.')
    parser.add_argument('--test-size', type=float, default=0.33,
                        help='Size of the test-set in percent.')
    parser.add_argument('--fptype', default='maccs', type=str,
                        help='Fingerprinter type.')
    parser.add_argument('--debug', default=False, action='store_true',
                        help='Enable debug mode')
    parser.add_argument('--mogon', default=False, action='store_true',
                        help='Use mogon mode (disables unsupported libraries).')

    opts = parser.parse_args()

    if opts.out[len(opts.out) - 1] != '/':
        opts.out += '/'

    return opts


if __name__ == '__main__':
    import warnings

    warnings.filterwarnings("ignore")
    main()
