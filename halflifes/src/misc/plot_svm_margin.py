
# Code source: Gaël Varoquaux
# Modified for documentation by Jaques Grobler
# License: BSD 3 clause

import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
import seaborn as sns
np.random.seed(42)
def plot_a():
    # we create 40 separable points
    np.random.seed(3)
    num_points = 20
    X = np.r_[np.random.randn(num_points, 2) - [2, 2], np.random.randn(num_points, 2) + [2, 2]]
    Y = [0] * num_points + [1] * num_points

    # figure number
    fignum = 1

    # fit the model
    for name, penalty in [('unreg', 10000)]:

        clf = svm.SVC(kernel='linear', C=penalty)
        clf.fit(X, Y)

        # get the separating hyperplane
        w = clf.coef_[0]
        a = -w[0] / w[1]
        xx = np.linspace(-5, 5)
        yy = a * xx - (clf.intercept_[0]) / w[1]

        # plot the parallels to the separating hyperplane that pass through the
        # support vectors
        margin = 1 / np.sqrt(np.sum(clf.coef_ ** 2))
        yy_down = yy + a * margin
        yy_up = yy - a * margin

        # plot the line, the points, and the nearest vectors to the plane
        plt.figure(fignum, figsize=(4, 3))
        plt.clf()

        plt.tick_params(
            axis='x',  # changes apply to the x-axis
            which='both',  # both major and minor ticks are affected
            bottom='off',  # ticks along the bottom edge are off
            top='off',  # ticks along the top edge are off
            labelbottom='off')  # labels along the bottom edge are off

        plt.tick_params(
            axis='y',  # changes apply to the x-axis
            which='both',  # both major and minor ticks are affected
            bottom='off',  # ticks along the bottom edge are off
            top='off',  # ticks along the top edge are off
            labelbottom='off')  # labels along the bottom edge are off

        plt.plot(xx, yy, 'k-')
        plt.plot(xx, yy_down, 'k--')
        plt.plot(xx, yy_up, 'k--')

        sup_vecs = clf.support_vectors_
        print(name, sup_vecs)
        plt.scatter(sup_vecs[:, 0], sup_vecs[:, 1], s=80,
                    facecolors='none',
                    edgecolors='black',
                    zorder=10
                    )
        plt.scatter(X[:, 0], X[:, 1], c=Y, zorder=10,
                    facecolors='none',
                    edgecolors='black',
                    cmap=plt.cm.Paired)

        plt.axis('tight')
        x_min = -4.8
        x_max = 4.2
        y_min = -6
        y_max = 6

        XX, YY = np.mgrid[x_min:x_max:200j, y_min:y_max:200j]
        Z = clf.predict(np.c_[XX.ravel(), YY.ravel()])

        # Put the result into a color plot
        Z = Z.reshape(XX.shape)
        plt.figure(fignum, figsize=(4, 3))
        # plt.pcolormesh(XX, YY, Z, cmap=plt.cm.Paired)

        plt.xlim(x_min, x_max)
        plt.ylim(y_min, y_max)

        plt.xticks(())
        plt.yticks(())
        fignum = fignum + 1


    plt.savefig('imgs/svm-margin.png')

def plot_b():
    import numpy as np
    import matplotlib.pyplot as plt
    from sklearn.datasets.samples_generator import make_blobs
    from sklearn.svm import SVC
    from sklearn.svm import LinearSVC
    sns.set(palette="deep", color_codes=True)
    sns.set_style("whitegrid", {'axes.grid' : False})
    sns.despine(left=True, right=True, top=True, bottom=True)

    # Let's generate some 2D data
    n_samples = 50
    n_features = 2
    centers = np.array([[-1., -1.], [1., 1.]])
    random_state = 0
    cluster_std = 0.5
    X, Y = make_blobs(n_samples, n_features, centers, cluster_std, random_state)

    # Let's first visualize the results
    levels = [-1.0, 0, 1.0]
    linestyles = ['dashed', 'solid', 'dashed']
    colors = 'k'

    def prep_plot(X, y):
        clf = SVC(C=1000, kernel='linear')
        clf.fit(X, y)
        w = clf.coef_
        b = clf.intercept_

        xx, yy = np.meshgrid(np.arange(-5, 5, 0.1), np.arange(-5, 5, 0.1))
        zz = np.dot(np.c_[xx.ravel(), yy.ravel()],
                    w.T) + b  # This is how we compute the classification score
        zz = zz.reshape(xx.shape)

        plt.figure(figsize=(6,4))

        plt.tick_params(
            axis='x',  # changes apply to the x-axis
            which='both',  # both major and minor ticks are affected
            bottom='off',  # ticks along the bottom edge are off
            top='off',  # ticks along the top edge are off
            labelbottom='off')  # labels along the bottom edge are off

        plt.tick_params(
            axis='y',  # changes apply to the x-axis
            which='both',  # both major and minor ticks are affected
            left='off',  # ticks along the bottom edge are off
            right='off',  # ticks along the top edge are off
            labelleft='off')  # labels along the bottom edge are off

        plt.box(on=None)

        plt.contour(xx, yy, zz, levels, colors=colors,
                    linestyles=linestyles)  # These are the gutters of the street

        plt.scatter(clf.support_vectors_[:, 0], clf.support_vectors_[:, 1],
                    s=150, facecolors='none',
                    edgecolors='black')
        plt.axis('tight')
        plt.axis([-2, 2, -2, 2])

        cols = sns.color_palette('deep')
        print(cols)
        print(y)
        y = [cols[5] if yi == 0 else cols[2] for yi in y]
        plt.scatter(X[:, 0], X[:, 1], c=y,
                    # facecolors='none',
                    edgecolors='black',
                    )


    prep_plot(X, Y)
    plt.savefig('imgs/svm-margin.png', dpi=300)

if __name__ == '__main__':
    plot_b()
